@extends('layouts.master')

@section('title', 'Dashboard | English Homestay')

@section('plugin_stylesheets')
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
@endsection

@section('stylesheets')
<style>
    @media (min-width: 768px) {

        #submit-btn-search {
            margin-top: 2rem;
        }
    }
</style>
@endsection

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-12 mb-3">
                <!-- Filter -->
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-edit"></i>
                            Filter
                        </h3>
                    </div>
                    <div class="card-body">
                        <form role="form" id="index-filter" action="{{ route('index') }}" method="'GET'">
                            <div class="row">
                                <div class="col-12 col-md-5">
                                    <div class="form-group">
                                        <label for="from-date">From Date</label>
                                        <div class="input-group date" id="from-date" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input"
                                                data-target="#from-date" name="from_date"
                                                value="{{ Request()->query('from_date') ?? \Carbon\Carbon::now()->firstOfMonth()->format('d/m/Y') }}" />
                                            <div class="input-group-append" data-target="#from-date"
                                                data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-5">
                                    <div class="form-group">
                                        <label for="to-date">To Date</label>
                                        <div class="input-group date" id="to-date" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input"
                                                data-target="#to-date" name="to_date"
                                                value="{{ Request()->query('to_date') ?? \Carbon\Carbon::today()->format('d/m/Y') }}" />
                                            <div class="input-group-append" data-target="#to-date"
                                                data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-2">
                                    <button id="submit-btn-search" type="submit"
                                        class="btn btn-primary btn-block">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End Filter -->
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Tổng profile mới</span>
                        <span class="info-box-number">
                            {{ $totalProfileCount }}
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-user-plus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Đã Đăng Ký</span>
                        <span class="info-box-number">{{ $registeredProfileCount }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-success elevation-1"><i class="fas fa-percent"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Tỉ lệ đăng ký</span>
                        <span
                            class="info-box-number">{{ !$totalProfileCount ? 'N/A' : round($registeredProfileCount/$totalProfileCount * 100, 2).'%'}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-user"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Tổng User</span>
                        <span class="info-box-number">{{ $userCount }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
</section>
@endsection


@section('plugin_scripts')
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/moment/moment.min.js') }}"></script>
<script
    src="{{ asset('vendors/admin-lte-3.0.5/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}">
</script>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $.fn.datetimepicker.Constructor.Default = $.extend(
        {},
        $.fn.datetimepicker.Constructor.Default,
        {
            icons: {
                time: "fas fa-clock",
                date: "fas fa-calendar",
                up: "fas fa-arrow-up",
                down: "fas fa-arrow-down",
                previous: "fas fa-arrow-circle-left",
                next: "fas fa-arrow-circle-right",
                today: "far fa-calendar-check-o",
                clear: "fas fa-trash",
                close: "far fa-times"
            }
        }
        );

        $("#from-date").datetimepicker({
            format: "DD/MM/YYYY"
        });

        $("#to-date").datetimepicker({
            format: "DD/MM/YYYY",
        });
    });
</script>
@endsection
