<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ route('index') }}" class="nav-link">Home</a>
        </li>

        @can('users_list')
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ route('user.index') }}" class="nav-link">User</a>
        </li>
        @endcan

        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ route('profile.index') }}" class="nav-link">Profile</a>
        </li>

        @can('accountants_list')
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ route('accountant.index') }}" class="nav-link">Accountant</a>
        </li>
        @endcan
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-user"></i>
                <span class="ml-2">{{ Auth::user()->name }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="{{ route('change_password') }}" class="dropdown-item">
                    Đổi mật khẩu
                </a>
                <a id="btn-logout" href="#" class="dropdown-item">
                    Đăng xuất
                </a>

                <form id="form-logout" action="{{ route('logout') }}" method="POST">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
