<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('index') }}" class="brand-link">
        <img src="{{ asset('vendors/admin-lte-3.0.5/img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light"><strong>English</strong> Homestay</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            {{-- <div class="image">
                <img src="{{ asset('vendors/admin-lte-3.0.5/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
            alt="User Image">
        </div> --}}
        <div class="info w-100">
            <a href="{{ route('index') }}" class="d-block text-center">{{ Auth::user()->name }}</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->

            @php $segment = Request()->segment(1); @endphp
            <li class="nav-item has-treeview">
                <a href="{{ route('index') }}" class="nav-link {{ empty($segment) ? 'active' : '' }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Dashboard
                    </p>
                </a>
            </li>
            @can('users_list')
            <li class="nav-item">
                <a href="{{ route('user.index') }}" class="nav-link {{ $segment == 'user' ? 'active' : '' }}">
                    <i class="nav-icon fas fa-user"></i>
                    <p>
                        User Management
                    </p>
                </a>
            </li>
            @endcan

            @php
            if($segment == 'profile') {
            $profileSegment = true;
            $statusId = Request()->query('status_id');
            $segment2 = Request()->segment(2);
            } else {
            $profileSegment = false;
            }
            @endphp

            <li class="nav-item has-treeview {{ $profileSegment && empty($segment2) ? 'menu-open' : '' }}">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                        Profile Management
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>

                <ul class="nav nav-treeview"
                    style="display: {{ $profileSegment && empty($segment2) ? 'block' : 'none' }}">
                    <li class="nav-item">
                        <a href="{{ route('profile.index') }}"
                            class="nav-link {{ $profileSegment && empty($segment2) && !$statusId ? 'active' : '' }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Tất cả</p>
                        </a>
                    </li>
                    @foreach(\App\Models\Profile::STATUS_LIST as $key => $status)
                    <li class="nav-item {{ $profileSegment && $statusId == $key ? 'active' : '' }}">
                        <a href="{{ route('profile.index', ['status_id' => $key]) }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>{{ $status }}</p>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </li>
            @can('profiles_create')
            <li class="nav-item has-treeview">
                <a href="{{ route('profile.create') }}"
                    class="nav-link {{ $profileSegment && $segment2 == 'create' ? 'active' : '' }}">
                    <i class="nav-icon fas fa-user-plus"></i>
                    <p>
                        Add Profile
                    </p>
                </a>
            </li>
            @endcan

            @can('profiles_import')
            <li class="nav-item has-treeview">
                <a href="{{ route('profile.import') }}"
                    class="nav-link {{ $profileSegment && $segment2 == 'import' ? 'active' : '' }}">
                    <i class="nav-icon fas fa-upload"></i>
                    <p>
                        Import From File
                    </p>
                </a>
            </li>
            @endcan

            @can('accountants_list')
            <li class="nav-item has-treeview">
                <a href="{{ route('accountant.index') }}" class="nav-link {{ $segment=='accountant' ? 'active' : '' }}">
                    <i class="nav-icon fas fa-money-bill-wave"></i>
                    <p>
                        Accoutant
                    </p>
                </a>
            </li>
            @endcan
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
