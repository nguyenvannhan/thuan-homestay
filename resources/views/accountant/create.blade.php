@extends('layouts.master')

@section('title', 'Accountants Create | English Homestay')

@section('plugin_stylesheets')
<link rel="stylesheet" href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<!-- summernote -->
<link rel="stylesheet" href="{{ asset('vendors/admin-lte-3.0.5/plugins/summernote/summernote-bs4.css') }}">
@endsection


@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Accountant
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('accountant.index') }}">Accountant</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- Filter -->
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            Tạo mới thu chi
                        </h3>
                    </div>
                    <div class="card-body">
                        <form role="form" method="POST" action="{{ route('accountant.store') }}"
                            id="accountant-create-form">
                            @csrf
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="money">Số tiền</label>
                                        <input id="money" type="text"
                                            class="form-control @error('money') is-invalid @enderror" name="money"
                                            value="{{ old('money') }}" required>
                                        @error('money')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="type-id">Type</label>
                                        <select class="select2bs4 form-control" name="type_id" id="type-id">
                                            <option value="{{ \App\Models\Accountant::INCOME_ID }}">Thu</option>
                                            <option value="{{ \App\Models\Accountant::OUTCOME_ID }}">Chi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="explain">Note</label>
                                        <textarea class="form-control" id="explain" name="explain"
                                            required>{{ old('explain') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('accountant.index') }}" type="button"
                                        class="btn btn-warning">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End Filter -->
            </div>
        </div>
    </div>
</section>
@endsection

@section('plugin_scripts')
<!-- Select2 -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/js/select2.full.min.js') }}"></script>

<!-- Summernote -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/summernote/summernote-bs4.min.js') }}"></script>
@endsection

@section('scripts')
<script src="{{ mix('js/accountant.js') }}"></script>
@endsection
