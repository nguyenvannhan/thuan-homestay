@extends('layouts.master')

@section('title', 'Accountants | English Homestay')

@section('plugin_stylesheets')
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

<!-- SweetAlert2 -->
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endsection

@section('stylesheets')
<style>
    #submit-btn-search {
        margin-bottom: 1rem;
    }

    @media (min-width: 768px) {

        #submit-btn-search,
        #reset-btn-search {
            margin-top: 2rem;
        }

        #submit-btn-search {
            margin-bottom: 0;
        }
    }
</style>
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Accountant <a href="{{ route('accountant.create') }}" class="btn btn-success btn-xs">Add New</a>
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="breadcrumb-item active">Accountant</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- Filter -->
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-edit"></i>
                            Filter
                        </h3>
                    </div>
                    <div class="card-body">
                        <form role="form" id="accountant-filter">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="from-date">From Date</label>
                                        <div class="input-group date" id="from-date" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input"
                                                data-target="#from-date" value="{{ $fromDate }}" required />
                                            <div class="input-group-append" data-target="#from-date"
                                                data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="to-date">To Date</label>
                                        <div class="input-group date" id="to-date" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input"
                                                data-target="#to-date" value="{{ $toDate }}" required />
                                            <div class="input-group-append" data-target="#to-date"
                                                data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <button id="submit-btn-search" type="submit"
                                                class="btn btn-primary btn-block">Submit</button>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <button id="reset-btn-search" type="button"
                                                class="btn btn-warning btn-block">Reset</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End Filter -->
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <div class="card-title w-100 d-block d-md-flex justify-content-between">
                            <p class="mb-0">Các khoản thu</p>
                            <p class="mb-0" id="sum-income"></p>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="income-datatable" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>Thời gian</th>
                                    <th>Nội dung</th>
                                    <th>Giá trị</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr class="text-center">
                                    <th>Thời gian</th>
                                    <th>Nội dung</th>
                                    <th>Giá trị</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="card card-warning">
                    <div class="card-header">
                        <div class="card-title w-100 d-block d-md-flex justify-content-between">
                            <p class="mb-0">Các khoản chi</p>
                            <p id="sum-outcome" class="mb-0"></p>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="outcome-datatable" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>Thời gian</th>
                                    <th>Nội dung</th>
                                    <th>Giá trị</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr class="text-center">
                                    <th>Thời gian</th>
                                    <th>Nội dung</th>
                                    <th>Giá trị</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form id="edit-profile-form">
                    <div id="overlay" class="overlay d-none justify-content-center align-items-center">
                        <i class="fas fa-2x fa-sync fa-spin"></i>
                    </div>
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Profile</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</section>
@endsection

@section('plugin_scripts')
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/moment/moment.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
</script>
<script
    src="{{ asset('vendors/admin-lte-3.0.5/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}">
</script>
<!-- SweetAlert2 -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
@endsection


@section('scripts')
@if(session('create_accountant'))
<script>
    window.createAccountant = true;
</script>
@endif
@if(session('update_accountant'))
<script>
    window.updateAccountant = true;
</script>
@endif

<script src="{{ mix('js/accountant.js') }}"></script>
@endsection
