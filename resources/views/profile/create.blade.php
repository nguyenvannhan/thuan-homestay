@extends('layouts.master')

@section('title', 'Profile Create | English Homestay')

@section('plugin_stylesheets')
<link rel="stylesheet" href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<!-- summernote -->
<link rel="stylesheet" href="{{ asset('vendors/admin-lte-3.0.5/plugins/summernote/summernote-bs4.css') }}">
@endsection


@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Profile
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('profile.index') }}">Profile</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- Filter -->
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            Add New Profile
                        </h3>
                    </div>
                    <div class="card-body">
                        <form role="form" method="POST" action="{{ route('profile.store') }}" id="profile-create-form">
                            @csrf
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="name">Họ tên</label>
                                        <input id="name" type="text"
                                            class="form-control @error('name') is-invalid @enderror" name="name"
                                            value="{{ old('name') }}" required>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" id="email"
                                            class="form-control @error('email') is-invalid @enderror"
                                            value="{{ old('email') }}">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <input type="text" name="phone" id="phone"
                                            class="form-control @error('phone') is-invalid @enderror"
                                            value="{{ old('phone') }}" placeholder="SDT1,SDT2;SDT3">
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="city">Tỉnh/Thành phố</label>
                                        <select class="select2bs4 form-control" name="city_id" id="city">
                                            <option value="0">Chọn Tỉnh/Thành phố</option>
                                            @foreach($cityList as $city)
                                            <option value="{{ $city->id }}"
                                                {{ $city->id === old('city_id') ? 'selected' : '' }}>
                                                {{ $city->display_name }}</option>ƒ
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="district">Quận/Huyện/Thị xã/TP</label>
                                        <select class="select2bs4 form-control" name="district_id" id="district"
                                            data-value="{{ old('district_id') }}">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label id="address">Địa chỉ</label>
                                        <input type="text" id="address" class="form-control" name="address"
                                            value="{{ old('address') }}">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Note</label>
                                        <textarea class="form-control" id="note"
                                            name="note">{{ old('note') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('profile.index') }}" type="button"
                                        class="btn btn-warning">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End Filter -->
            </div>
        </div>
    </div>
</section>
@endsection

@section('plugin_scripts')
<!-- Select2 -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/js/select2.full.min.js') }}"></script>

<!-- Summernote -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/summernote/summernote-bs4.min.js') }}"></script>
@endsection

@section('scripts')
<script src="{{ mix('js/profile.js') }}"></script>
@endsection
