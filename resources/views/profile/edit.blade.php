<div class="row">
    <div class="col-12 bg-danger text-white d-none" id="edit-profile-errors">

    </div>
    <div class="col-12 col-md-3">
        <div class="form-group">
            <label>ID</label>
            <input class="form-control" name="id" value="{{ $profile->id }}" disabled>
        </div>
    </div>
    <div class="col-12 col-md-3">
        <div class="form-group">
            <label>Họ tên</label>
            <input class="form-control" name="name" value="{{ $profile->name }}">
        </div>
    </div>
    <div class="col-12 col-md-3">
        <div class="form-group">
            <label>Email</label>
            <input class="form-control" name="email" value="{{ $profile->email }}">
        </div>
    </div>
    <div class="col-12 col-md-3">
        <div class="form-group">
            <label>SĐT</label>
            <input class="form-control" name="phone" value="{{ $profile->phone }}">
        </div>
    </div>
    <div class="col-12 col-md-3">
        <div class="form-group">
            <label>Thành phố/Tỉnh</label>
            <select class="form-control select2bs4" name="city_id" id="city">
                <option value="0">Chọn Thành phố/Tỉnh</option>
                @foreach($cityList as $city)
                <option value="{{ $city->id }}"
                    {{ $profile->district && $profile->district->city_id == $city->id ? 'selected' : '' }}>
                    {{ $city->display_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-12 col-md-3">
        <div class="form-group">
            <label>Quận/Huyện/Thị xã</label>
            <select class="form-control select2bs4" name="district_id" id="district"
                data-value="{{ $profile->district_id }}">
                <option value="0">Quận/Huyện/Thị xã</option>
                @foreach($districtList as $district)
                <option value="{{ $district->id }}" {{ $profile->district_id == $district->id ? 'selected' : '' }}>
                    {{ $district->display_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="form-group">
            <label>Địa chỉ</label>
            <input class="form-control" name="address" value="{{ $profile->address }}">
        </div>
    </div>


    <div class="col-12 col-md-6">
        <div class="form-group">
            <label>Trạng thái</label>
            <select class="form-control edit-select2" name="status_id"
                data-registered="{{ \App\Models\Profile::DA_DANG_KY }}">
                @foreach(\App\Models\Profile::STATUS_LIST as $key => $status)
                <option value="{{ $key }}" {{ $key == $profile->status_id ? 'selected' : '' }}>{{ $status }}
                </option>
                @endforeach
            </select>
            <p class="text-danger" id="deposit-change-status"></p>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="form-group">
            <label for="edit-appointment-date">Đặt lịch hẹn</label>
            <div class="input-group date" id="edit-appointment-date" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" name="appointment_time"
                    data-target="#edit-appointment-date"
                    value="{{ $profile->appointment_time ? $profile->appointment_time->format('d/m/Y H:i') : '' }}" />
                <div class="input-group-append" data-target="#edit-appointment-date" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
    </div>
    @if($profile->deposited_fee)
    <div class="col-12 text-info">
        <span>{{ $profile->name }} đã đặt cọc <strong>{{ number_format($profile->deposited_fee->money, 0, '.', ',') }}
                VNĐ</strong></span>
        @if(!empty($profile->deposited_fee->note))
        <br>
        <p>{!! $profile->deposited_fee->note !!}</p>
        @endif
    </div>
    @endif
    <div class="col-12">
        <div class="form-group">
            <label>Notes</label>
            <textarea class="form-control" id="edit-profile-note" name="note">
                    {!! $profile->note !!}
                </textarea>
        </div>
    </div>
</div>
