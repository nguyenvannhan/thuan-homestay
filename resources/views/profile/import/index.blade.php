@extends('layouts.master')

@section('title', 'Profile Import | English Homestay')

@section('plugin_stylesheets')
<!-- DataTables -->
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<!-- SweetAlert2 -->
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endsection

@section('stylesheets')
<style>
    #common-error p:last-of-type {
        margin-bottom: 0 !important;
    }
</style>
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Nhập liệu
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('profile.index') }}">Profile</a></li>
                    <li class="breadcrumb-item active">Import</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-danger d-none" id="common-error">
                        </div>
                        <form id="upload-file-form">
                            <div class="form-group">
                                <label for="exampleInputFile">File input</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile" required
                                            accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                    <div class="input-group-append">
                                        <button type="submit" class="input-group-text btn-primary bg-primary"
                                            id="upload-submit">Preview</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3 d-none text-center" id="loading">
                <p class="text-center">
                    <i class="fas fa-spinner fa-spin fa-4x"></i>
                    <br>
                    Loading
                </p>
            </div>
            <div id="file-content" class="col-12">

            </div>
        </div>
    </div>
</section>
@endsection

@section('plugin_scripts')
<!-- DataTables -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
</script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
</script>
<!-- Select2 -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/js/select2.full.min.js') }}"></script>

<!-- SweetAlert2 -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
@endsection


@section('scripts')
<script src="{{ mix('js/profile-import.js') }}"></script>
@endsection
