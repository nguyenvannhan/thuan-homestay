@extends('layouts.master')

@section('title', 'User Create | English Homestay')

@section('plugin_stylesheets')
<link rel="stylesheet" href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<!-- summernote -->
<link rel="stylesheet" href="{{ asset('vendors/admin-lte-3.0.5/plugins/summernote/summernote-bs4.css') }}">
@endsection


@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    User
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('user.index') }}">User</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- Filter -->
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            Add New User
                        </h3>
                    </div>
                    <div class="card-body">
                        <form role="form" method="POST" action="{{ route('user.store') }}" id="user-create-form">
                            @csrf
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="name">Họ tên</label>
                                        <input id="name" type="text"
                                            class="form-control @error('name') is-invalid @enderror" name="name"
                                            value="{{ old('name') }}" required>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" id="email"
                                            class="form-control @error('email') is-invalid @enderror"
                                            value="{{ old('email') }}" required>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="text" name="password" id="password" class="form-control"
                                            value="{{ old('passowrd') ?? Str::random(10) }}" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="role">Quyền hạn</label>
                                        <div class="w-100">
                                            @foreach($roleList as $role)
                                            <p class="d-block bg-light py-2 px-3 rounded">
                                                {{ ucwords(__('roles.display_name.'.$role->name)) }}</p>

                                            <div class="py-2 px-3 d-flex mb-3">
                                                @foreach($role->permissions as $permission)
                                                <div class="form-check form-check-inline mr-3">
                                                    <input class="form-check-input" type="checkbox"
                                                        name="permission_ids[]" id="{{ $permission->name }}"
                                                        value="{{ $permission->id }}">
                                                    <label class="form-check-label"
                                                        for="{{ $permission->name }}">{{ ucwords(__('permissions.display_name.'.$permission->name)) }}</label>
                                                </div>
                                                @endforeach
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('profile.index') }}" type="button"
                                        class="btn btn-warning">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End Filter -->
            </div>
        </div>
    </div>
</section>
@endsection

@section('plugin_scripts')
<!-- Select2 -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/js/select2.full.min.js') }}"></script>

<!-- Summernote -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/summernote/summernote-bs4.min.js') }}"></script>
@endsection

@section('scripts')
<script src="{{ mix('js/user.js') }}"></script>
@endsection
