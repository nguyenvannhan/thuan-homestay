<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="admin-lte-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <link rel="icon" type="image/png" href="{{ url('vendors/admin-lte-3.0.5/img/AdminLTELogo.png') }}">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('/vendors/admin-lte-3.0.5/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet"
        href="{{ asset('/vendors/admin-lte-3.0.5/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">

    @yield('plugin_stylesheets')
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/vendors/admin-lte-3.0.5/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    @yield('stylesheets')
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <div class="wrapper">
        @include('components.navbar')

        @include('components.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- /.content-wrapper -->
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <strong>Copyright &copy; {{ date('Y') }} <a href="{{ route('index') }}">English HomeStay</a>.</strong>
            All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('/vendors/admin-lte-3.0.5/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('/vendors/admin-lte-3.0.5/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('/vendors/admin-lte-3.0.5/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}">
    </script>

    @yield('plugin_scripts')

    <!-- AdminLTE App -->
    <script src="{{ asset('/vendors/admin-lte-3.0.5/js/adminlte.js') }}"></script>

    <script>
        var rootUrl = "{{ url('/') }}";

        $(document).ready(function() {
            $('#btn-logout').on('click', function(e) {
                e.preventDefault();

                $('#form-logout').trigger('submit');
            });
        });
    </script>

    @yield('scripts')
</body>

</html>
