$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="admin-lte-token"]').attr("content")
    }
});

$(document).ready(function() {
    $(".select2bs4").select2({
        theme: "bootstrap4",
        minimumResultsForSearch: -1
    });

    if (!$("#user-create-form").length) {
        const Toast = Swal.mixin({
            toast: true,
            position: "top-right",
            showConfirmButton: false,
            timer: 5000
        });

        if (window.createUser) {
            Toast.fire({
                icon: "success",
                title: '<span class="ml-2">Create User Successfully!</span>'
            });
        }

        if (window.updateUser) {
            Toast.fire({
                icon: "success",
                title: '<span class="ml-2">Update User Successfully!</span>'
            });
        }

        let table = $("#user-datatable").DataTable({
            dom:
                "<'row'<'col-sm-12 col-md-4'i><'col-sm-12 col-md-2'l><'col-sm-12 col-md-6'p>r>" +
                "<'row'<'col-12't>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
            paging: true,
            ordering: true,
            autoWidth: false,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: rootUrl + "/users/data-table",
                method: "POST",
                data: function(d) {
                    d.search_keyword = $("#search-keyword").val();
                    d.permission_ids = $("#permission-ids").val();
                }
            },
            columns: [
                { data: "id", name: "id", className: "text-center" },
                { data: "name", name: "name" },
                { data: "email", name: "email" },
                {
                    data: "role",
                    name: "role",
                    className: "text-center",
                    orderable: false,
                    searchable: false
                },
                {
                    data: "status",
                    name: "status",
                    className: "text-center",
                    searchable: false
                },
                {
                    data: "timestamps",
                    name: "timestamps",
                    orderable: false,
                    searchable: false
                },
                {
                    data: "action",
                    name: "action",
                    className: "text-center",
                    orderable: false,
                    searchable: false
                }
            ],
            order: [[0, "DESC"]]
        });

        $("#user-filter").on("submit", function(e) {
            table.draw();
            e.preventDefault();
        });

        $("#reset-btn-search").on("click", function() {
            $("#user-filter").trigger("reset");

            table.draw();
        });

        $(document).on("click", ".btn-status", function(e) {
            let btn = $(this);
            let id = btn.data("id");

            $.ajax({
                url: rootUrl + "/users/change-status",
                method: "POST",
                data: {
                    id: id
                },
                beforeSend: function() {
                    btn.html('<i class="fas fa-spinner fa-spin"></i>');
                }
            })
                .done(function(data) {
                    Toast.fire({
                        icon: "success",
                        title:
                            '<span class="ml-2">Update Status Successfully!</span>'
                    });

                    if (data.status) {
                        btn.removeClass("btn-danger")
                            .addClass("btn-success")
                            .html("Active");
                    } else {
                        btn.removeClass("btn-success")
                            .addClass("btn-danger")
                            .html("Disabled");
                    }
                })
                .fail(function(error) {
                    Toast.fire({
                        icon: "error",
                        title: '<span class="ml-2">Update Status Error!</span>'
                    });

                    if (btn.hasClass("btn-danger")) {
                        btn.html("Disabled");
                    } else {
                        btn.html("Active");
                    }
                });
        });

        $(document).on("click", ".btn-reset-pass", function(e) {
            e.preventDefault();

            let id = $(this).data("id");

            Swal.fire({
                title: "Reset Password",
                text: "Do you want to reset password for selected user?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                preConfirm: () => {
                    return new Promise((resolve, reject) => {
                        $.ajax({
                            url: rootUrl + "/users/reset-password",
                            method: "POST",
                            data: {
                                id: id
                            }
                        })
                            .done(function(data) {
                                if (data.success) {
                                    Swal.fire({
                                        title: "Reset Password",
                                        html:
                                            "<p>Reset Password Successfully!<br>New Password<br><strong>" +
                                            data.password +
                                            "</p>",
                                        icon: "success"
                                    });
                                } else {
                                    Swal.fire({
                                        title: "Reset Password",
                                        text: "Reset Password Failed!",
                                        icon: "error"
                                    });
                                }
                            })
                            .fail(function(error) {
                                Swal.fire({
                                    title: "Reset Password",
                                    text: "Reset Password Failed!",
                                    icon: "error"
                                });
                            });
                    });
                }
            });
        });
    }
});
