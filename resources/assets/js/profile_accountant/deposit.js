export const depositProfile = (table, Toast) => {
    $("#deposit-profile-explain").summernote({
        height: 200,
        toolbar: [
            // [groupName, [list of button]]
            ["style", ["bold", "italic", "underline", "clear"]],
            ["fontsize", ["fontsize"]],
            ["color", ["color"]]
        ]
    });

    $(document).on("click", ".btn-deposit", function(e) {
        e.preventDefault();

        let id = $(this).data("id");

        $("#modal-deposit")
            .find('input[name="profile_id"]:hidden')
            .val(id);
        $("#modal-deposit")
            .find('input[name="accountant_id"]:hidden')
            .val(0);

        $("#modal-deposit form").trigger("reset");
        $("#modal-deposit #deposit-profile-explain").summernote("reset");

        $("#modal-deposit").modal("show");
    });

    $(document).on("submit", "#deposit-profile-form", function(e) {
        e.preventDefault();

        $("#modal-deposit #overlay")
            .removeClass("d-none")
            .addClass("d-flex");

        let data = $(this).serializeArray();
        let id = $(this)
            .find('input[name="accountant_id"]')
            .val();

        $.ajax({
            url: rootUrl + "/accountants/ajax/deposit",
            method: "POST",
            data: data
        })
            .done(function(data) {
                table.ajax.reload(null, false);

                Toast.fire({
                    icon: "success",
                    title: '<span class="ml-2">Đặt cọc thành công</span>'
                });

                $("#modal-deposit").modal("hide");
                setupForm({
                    profile_id: 0,
                    accountant_id: 0,
                    money: 0,
                    explain: ""
                });
                $("#modal-deposit form").trigger("reset");
                $("#modal-deposit #deposit-profile-explain").summernote(
                    "reset"
                );

                $("#modal-deposit #overlay")
                    .removeClass("d-flex")
                    .addClass("d-none");
            })
            .fail(function(error) {
                Toast.fire({
                    icon: "error",
                    title: '<span class="ml-2">Đặt cọc thất bại!</span>'
                });

                $("#modal-deposit #overlay")
                    .removeClass("d-flex")
                    .addClass("d-none");
            });
    });
};

export const seeDepositDetail = () => {
    $(document).on("click", ".deposit-edit", function(e) {
        e.preventDefault();

        let accountantID = $(this).data("accountant-id");
        let profileID = $(this).data("profile-id");

        $("#modal-deposit #overlay")
            .removeClass("d-none")
            .addClass("d-flex");

        $("#modal-deposit").modal("show");

        $.ajax({
            url: rootUrl + "/accountants/ajax/deposit",
            method: "GET",
            data: {
                profile_id: profileID,
                accountant_id: accountantID
            }
        })
            .done(function(data) {
                setupForm(data);
            })
            .fail(function(error) {
                $("#modal-deposit")
                    .find(".modal-body")
                    .html('<p class="text-center">LOAD ERROR!!!</p>');
            })
            .always(function() {
                $("#modal-deposit #overlay")
                    .removeClass("d-flex")
                    .addClass("d-none");
            });
    });
};

function setupForm(data) {
    $("#modal-deposit")
        .find('input[name="profile_id"]')
        .val(data.profile_id);
    $("#modal-deposit")
        .find('input[name="accountant_id"]')
        .val(data.id);

    $("#modal-deposit")
        .find('input[name="money"]')
        .val(data.money);

    $("#modal-deposit")
        .find("#deposit-profile-explain")
        .summernote("code", data.explain);
}
