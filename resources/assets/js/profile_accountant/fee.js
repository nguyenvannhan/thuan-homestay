export const feeProfile = (table, Toast) => {
    $("#fee-profile-explain").summernote({
        height: 200,
        toolbar: [
            // [groupName, [list of button]]
            ["style", ["bold", "italic", "underline", "clear"]],
            ["fontsize", ["fontsize"]],
            ["color", ["color"]]
        ]
    });

    $(document).on("click", ".btn-fee", function(e) {
        e.preventDefault();

        let id = $(this).data("id");

        $("#modal-fee")
            .find('input[name="profile_id"]:hidden')
            .val(id);

        $("#modal-fee form").trigger("reset");
        $("#modal-fee #fee-profile-explain").summernote("reset");

        $("#modal-fee").modal("show");
    });

    $(document).on("submit", "#fee-profile-form", function(e) {
        e.preventDefault();

        $("#modal-fee #overlay")
            .removeClass("d-none")
            .addClass("d-flex");

        let data = $(this).serializeArray();
        let id = $(this)
            .find('input[name="profile_id"]')
            .val();

        $.ajax({
            url: rootUrl + "/profiles/fee",
            method: "POST",
            data: data
        })
            .done(function(data) {
                table.ajax.reload(null, false);

                Toast.fire({
                    icon: "success",
                    title: '<span class="ml-2">Đóng học phí thành công</span>'
                });

                $("#modal-fee").modal("hide");
                $("#modal-fee form").trigger("reset");
                $("#modal-fee #fee-profile-explain").summernote("reset");

                $("#modal-fee #overlay")
                    .removeClass("d-flex")
                    .addClass("d-none");
            })
            .fail(function(error) {
                Toast.fire({
                    icon: "error",
                    title: '<span class="ml-2">Đóng học phí thất bại!</span>'
                });

                $("#modal-fee #overlay")
                    .removeClass("d-flex")
                    .addClass("d-none");
            });
    });
};

export const seeFeeDetail = () => {
    $(document).on("click", ".fee-detail", function(e) {
        e.preventDefault();

        let profileId = $(this).data("profile-id");

        $("#modal-fee-detail").modal("show");

        $("#modal-fee-detail #overlay")
            .removeClass("d-none")
            .addClass("d-flex");

        $.ajax({
            url: rootUrl + "/accountants/profiles/" + profileId,
            method: "POST"
        })
            .done(function(data) {
                $("#modal-fee-detail")
                    .find(".modal-body")
                    .html(data.view);
            })
            .fail(function(error) {
                $("#modal-fee-detail")
                    .find(".modal-body")
                    .html('<p class="text-center">LOAD ERROR!!!</p>');
            })
            .always(function() {
                $("#modal-fee-detail #overlay")
                    .removeClass("d-flex")
                    .addClass("d-none");
            });
    });

    $("#modal-fee-detail").on("hidden.bs.modal", function() {
        $(this)
            .find(".modal-body")
            .html("");
    });
};
