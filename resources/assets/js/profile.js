import { depositProfile, seeDepositDetail } from "./profile_accountant/deposit";
import { feeProfile, seeFeeDetail } from "./profile_accountant/fee";

$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="admin-lte-token"]').attr("content")
    }
});

$(document).ready(function() {
    $(".select2bs4").select2({
        theme: "bootstrap4"
    });

    if ($("#city").length && $("#district").length) {
        getDistrictList($("#city").val());
    }

    $(document).on("change", "#city", function(e) {
        let cityId = $(this).val();

        getDistrictList(cityId);
    });

    if (!$("#profile-create-form").length) {
        $.fn.datetimepicker.Constructor.Default = $.extend(
            {},
            $.fn.datetimepicker.Constructor.Default,
            {
                icons: {
                    time: "fas fa-clock",
                    date: "fas fa-calendar",
                    up: "fas fa-arrow-up",
                    down: "fas fa-arrow-down",
                    previous: "fas fa-arrow-circle-left",
                    next: "fas fa-arrow-circle-right",
                    today: "far fa-calendar-check-o",
                    clear: "fas fa-trash",
                    close: "far fa-times"
                }
            }
        );

        const Toast = Swal.mixin({
            toast: true,
            position: "bottom",
            showConfirmButton: false,
            timer: 5000
        });

        if (window.createProfile) {
            Toast.fire({
                icon: "success",
                title:
                    '<span class="ml-2">Create New Profile Successfully</span>'
            });
        }

        let statusId = getParamFromCurrentURL().get("status_id") ?? null;
        //Date range picker
        $("#from-date").datetimepicker({
            format: "DD/MM/YYYY"
        });

        $("#to-date").datetimepicker({
            format: "DD/MM/YYYY",
            useCurrent: false
        });
        $("#from-date").on("change.datetimepicker", function(e) {
            $("#to-date").datetimepicker("minDate", e.date);
        });
        $("#to-date").on("change.datetimepicker", function(e) {
            $("#from-date").datetimepicker("maxDate", e.date);
        });

        let table = $("#profile-datatable").DataTable({
            dom:
                "<'row'<'col-sm-12 col-md-4'i><'col-sm-12 col-md-2'l><'col-sm-12 col-md-6'p>r>" +
                "<'row'<'col-12't>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
            paging: true,
            ordering: true,
            autoWidth: false,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: rootUrl + "/profiles/data-table",
                method: "POST",
                data: function(d) {
                    d.from_date = $("#from-date input").val();
                    d.to_date = $("#to-date input").val();
                    d.marketer_id = $("#marketer-id").val();
                    d.phone = $("#phone").val();
                    d.status = statusId;
                }
            },
            columns: [
                { data: "id", name: "id" },
                { data: "profile", name: "profile", orderable: false },
                { data: "appointment_time", name: "appointment_time" },
                { data: "note", name: "note", orderable: false },
                {
                    data: "status",
                    name: "status",
                    orderable: false,
                    className: "text-center"
                },
                { data: "author", name: "author", orderable: false },
                { data: "action", name: "action", orderable: false }
            ],
            order: [[0, "DESC"]]
        });

        $("#profile-filter").on("submit", function(e) {
            table.draw();

            e.preventDefault();
        });

        $("#reset-btn-search").on("click", function() {
            $("#profile-filter").trigger("reset");

            table.draw();
        });

        editProfile(table, Toast);
        deleteProfile(table, Toast);

        feeProfile(table, Toast);
        seeFeeDetail();

        depositProfile(table, Toast);
        seeDepositDetail();
    } else {
        $("#note").summernote({
            height: 200,
            toolbar: [
                // [groupName, [list of button]]
                ["style", ["bold", "italic", "underline", "clear"]],
                ["fontsize", ["fontsize"]],
                ["color", ["color"]]
            ]
        });
    }
});

function getParamFromCurrentURL() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    return urlParams;
}

function getDistrictList(cityId) {
    let currentValue = $("#district").data("value");
    $.ajax({
        url: rootUrl + `/district/get-from-city/${cityId}`,
        method: "GET"
    })
        .done(function(data) {
            let optionHTML = data.district_list.reduce((html, item) => {
                return (
                    html +
                    `<option value="${item.id}"${
                        currentValue == item.id ? "selected" : ""
                    }>${item.display_name}</option>`
                );
            }, '<option value="0">Lựa chọn Quận/Huyện/Thị xã/TP</option>');

            $("#district").html(optionHTML);
            $("#district")
                .select2("destroy")
                .select2({
                    theme: "bootstrap4"
                });
        })
        .fail(function(error) {
            $("#district")
                .select2("destroy")
                .select2({
                    theme: "bootstrap4"
                });
        });
}

function editProfile(table, Toast) {
    $(document).on("click", ".btn-edit", function(e) {
        e.preventDefault();

        let id = $(this).data("id");

        $.ajax({
            url: rootUrl + "/profiles/edit/" + id,
            method: "GET",
            beforeSend: function() {
                $("#modal-edit #overlay")
                    .removeClass("d-none")
                    .addClass("d-flex");
                $("#modal-edit").modal("show");
            },
            complete: function() {
                $("#modal-edit #overlay")
                    .removeClass("d-flex")
                    .addClass("d-none");
            }
        })
            .done(function(data) {
                $("#modal-edit")
                    .find(".modal-body")
                    .html(data.view);

                $("#edit-profile-form select").select2({
                    theme: "bootstrap4",
                    minimumResultsForSearch: -1
                });

                $("#edit-appointment-date").datetimepicker({
                    format: "DD/MM/YYYY HH:mm"
                });

                $("#edit-profile-note").summernote({
                    height: 200,
                    toolbar: [
                        // [groupName, [list of button]]
                        ["style", ["bold", "italic", "underline", "clear"]],
                        ["fontsize", ["fontsize"]],
                        ["color", ["color"]]
                    ]
                });
            })
            .fail(function(e) {
                $("#modal-edit")
                    .find(".modal-body")
                    .html("");
                $("#modal-edit").modal("show");
            });
    });

    $(document).on("submit", "#edit-profile-form", function(e) {
        e.preventDefault();

        $("#modal-edit #overlay")
            .removeClass("d-none")
            .addClass("d-flex");

        let id = $(this)
            .find('input[name="id"]')
            .val();

        let data = $(this).serializeArray();

        $.ajax({
            url: rootUrl + "/profiles/edit/" + id,
            method: "POST",
            data: data
        })
            .done(function(data) {
                table.ajax.reload(null, false);

                Toast.fire({
                    icon: "success",
                    title:
                        '<span class="ml-2">Update Profile Successfully</span>'
                });

                $("#modal-edit").modal("hide");
                $("#modal-edit .modal-body").html("");
                $("#modal-edit #overlay")
                    .removeClass("d-flex")
                    .addClass("d-none");
            })
            .fail(function(error) {
                if (error.status === 422) {
                    let errorMessages = error.responseJSON.errors;

                    let htmlContent = "";
                    for (let errorItem in errorMessages) {
                        htmlContent += errorMessages[errorItem].reduce(
                            (content, errorMess) => {
                                return (
                                    content + `<span>${errorMess}</span><br>`
                                );
                            },
                            ""
                        );
                    }

                    $("#edit-profile-errors")
                        .html(htmlContent)
                        .removeClass("d-none");
                }

                $("#modal-edit #overlay")
                    .removeClass("d-flex")
                    .addClass("d-none");

                Toast.fire({
                    icon: "error",
                    title: '<span class="ml-2">Update Profile failed!</span>'
                });
            });
    });

    $(document).on(
        "change",
        '#edit-profile-form select[name="status_id"]',
        function(e) {
            if ($(this).val() == $(this).data("registered")) {
                $("#deposit-change-status").text(
                    "Phần đặt cọc (nếu có) của profile sẽ chuyển thành học phí"
                );
            } else {
                $("#deposit-change-status").text("");
            }
        }
    );
}

function deleteProfile(table, Toast) {
    $(document).on("click", ".btn-delete", function(e) {
        e.preventDefault();

        let profileId = $(this).data("id");

        Swal.fire({
            title: "<strong>Delete Profile</strong>",
            icon: "info",
            html: "<span>Do you really want to delete profile?</span>",
            showCancelButton: true,
            showConfirmButton: true,
            showLoading: true,
            preConfirm: function() {
                return new Promise((resolve, reject) => {
                    $.ajax({
                        url: rootUrl + "/profiles/delete",
                        method: "POST",
                        data: {
                            id: profileId
                        }
                    })
                        .then(function(data) {
                            if (data) {
                                table.ajax.reload(null, false);

                                Toast.fire({
                                    icon: "success",
                                    title:
                                        '<span class="ml-2">Delete Profile Successfully</span>'
                                });
                            } else {
                                Toast.fire({
                                    icon: "error",
                                    title:
                                        '<span class="ml-2">Delete Profile failed!</span>'
                                });
                            }
                        })
                        .catch(function(err) {
                            Toast.fire({
                                icon: "error",
                                title:
                                    '<span class="ml-2">Delete Profile failed!</span>'
                            });
                        });
                });
            }
        });
    });
}
