$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="admin-lte-token"]').attr("content")
    }
});

$(document).ready(function() {
    if (!$("#accountant-create-form").length) {
        $.fn.datetimepicker.Constructor.Default = $.extend(
            {},
            $.fn.datetimepicker.Constructor.Default,
            {
                icons: {
                    time: "fas fa-clock",
                    date: "fas fa-calendar",
                    up: "fas fa-arrow-up",
                    down: "fas fa-arrow-down",
                    previous: "fas fa-arrow-circle-left",
                    next: "fas fa-arrow-circle-right",
                    today: "far fa-calendar-check-o",
                    clear: "fas fa-trash",
                    close: "far fa-times"
                }
            }
        );

        //Date range picker
        $("#from-date").datetimepicker({
            format: "DD/MM/YYYY"
        });

        $("#to-date").datetimepicker({
            format: "DD/MM/YYYY",
            useCurrent: false
        });
        $("#from-date").on("change.datetimepicker", function(e) {
            $("#to-date").datetimepicker("minDate", e.date);
        });
        $("#to-date").on("change.datetimepicker", function(e) {
            $("#from-date").datetimepicker("maxDate", e.date);
        });

        loadSum();

        const Toast = Swal.mixin({
            toast: true,
            position: "bottom",
            showConfirmButton: false,
            timer: 5000
        });

        if (window.createAccountant) {
            Toast.fire({
                icon: "success",
                title:
                    '<span class="ml-2">Create New Accountant Successfully</span>'
            });
        }
        if (window.updateAccountant) {
            Toast.fire({
                icon: "success",
                title:
                    '<span class="ml-2">Update Accountant Successfully</span>'
            });
        }

        let incomeTable = $("#income-datatable").DataTable({
            paging: true,
            ordering: true,
            autoWidth: false,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: rootUrl + "/accountants/income/data-table",
                method: "POST",
                data: function(d) {
                    d.from_date = $("#from-date input").val();
                    d.to_date = $("#to-date input").val();
                }
            },
            columns: [
                {
                    data: "created_at",
                    name: "created_at",
                    className: "text-center"
                },
                { data: "content", name: "content", orderable: false },
                { data: "money", name: "money", className: "text-right" },
                {
                    data: "action",
                    name: "action",
                    className: "text-center",
                    orderable: false,
                    searchable: false
                }
            ],
            order: [[0, "DESC"]]
        });

        let outcomeTable = $("#outcome-datatable").DataTable({
            paging: true,
            ordering: true,
            autoWidth: false,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: rootUrl + "/accountants/outcome/data-table",
                method: "POST",
                data: function(d) {
                    d.from_date = $("#from-date input").val();
                    d.to_date = $("#to-date input").val();
                }
            },
            columns: [
                {
                    data: "created_at",
                    name: "created_at",
                    className: "text-center"
                },
                { data: "content", name: "content", orderable: false },
                { data: "money", name: "money", className: "text-right" },
                {
                    data: "action",
                    name: "action",
                    className: "text-center",
                    orderable: false,
                    searchable: false
                }
            ],
            order: [[0, "DESC"]]
        });

        $("#accountant-filter").on("submit", function(e) {
            incomeTable.draw();
            outcomeTable.draw();

            loadSum();

            e.preventDefault();
        });

        $("#reset-btn-search").on("click", function() {
            $("#accountant-filter").trigger("reset");

            incomeTable.draw();
            outcomeTable.draw();

            loadSum();
        });

        deleteAccountant(incomeTable, outcomeTable, Toast);
    } else {
        $(".select2bs4").select2({
            theme: "bootstrap4",
            minimumResultsForSearch: -1
        });

        $("#explain").summernote({
            height: 200,
            toolbar: [
                // [groupName, [list of button]]
                ["style", ["bold", "italic", "underline", "clear"]],
                ["fontsize", ["fontsize"]],
                ["color", ["color"]]
            ]
        });
    }
});

function getParamFromCurrentURL() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    return urlParams;
}

function loadSum() {
    $.ajax({
        url: rootUrl + "/accountants/get-sum",
        method: "POST",
        data: {
            from_date: $("#from-date input").val(),
            to_date: $("#to-date input").val()
        }
    }).done(function(data) {
        $("#sum-income").html(data.sumOfIncome);
        $("#sum-outcome").html(data.sumOfOutcome);
    });
}

function deleteAccountant(incomeTable, outcomeTable, Toast) {
    $(document).on("click", ".btn-delete", function(e) {
        e.preventDefault();

        let accountantId = $(this).data("id");

        Swal.fire({
            title: "<strong>Delete Profile</strong>",
            icon: "info",
            html: "<span>Do you really want to delete selected?</span>",
            showCancelButton: true,
            showConfirmButton: true,
            showLoading: true,
            preConfirm: function() {
                return new Promise((resolve, reject) => {
                    $.ajax({
                        url: rootUrl + "/accountants/delete",
                        method: "POST",
                        data: {
                            id: accountantId
                        }
                    })
                        .then(function(data) {
                            if (data) {
                                incomeTable.ajax.reload(null, false);
                                outcomeTable.ajax.reload(null, false);

                                Toast.fire({
                                    icon: "success",
                                    title:
                                        '<span class="ml-2">Delete Successfully</span>'
                                });
                            } else {
                                Toast.fire({
                                    icon: "error",
                                    title:
                                        '<span class="ml-2">Delete failed!</span>'
                                });
                            }
                        })
                        .catch(function(err) {
                            Toast.fire({
                                icon: "error",
                                title:
                                    '<span class="ml-2">Delete failed!</span>'
                            });
                        });
                });
            }
        });
    });
}
