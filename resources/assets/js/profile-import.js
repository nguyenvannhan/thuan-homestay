$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="admin-lte-token"]').attr("content")
    }
});

$(document).ready(function() {
    bsCustomFileInput.init();

    $("#upload-file-form").trigger("reset");

    $("#upload-file-form").on("submit", function(e) {
        e.preventDefault();
        $("#common-error")
            .removeClass("d-block")
            .addClass("d-none");
        $("#loading")
            .removeClass("d-none")
            .addClass("d-block");

        $('#file-content').html('');

        let formData = new FormData();
        formData.append("file", $("#exampleInputFile")[0].files[0]);

        $.ajax({
            url: rootUrl + "/profiles/import",
            method: "POST",
            data: formData,
            contentType: false,
            processData: false
        })
            .always(function() {
                $("#loading")
                    .removeClass("d-block")
                    .addClass("d-none");
            })
            .done(function(data) {
                if (data.success) {
                    window.profileList = data.profileList;

                    $("#file-content").html(data.html);

                    $("#file-content-table").DataTable();

                    $("#city").select2({
                        theme: "bootstrap4"
                    });
                    $("#district").select2({
                        theme: "bootstrap4"
                    });
                } else {
                    $("#common-error").html("<p>" + data.message + "</p>");
                    $("#common-error")
                        .removeClass("d-none")
                        .addClass("d-block");
                }
            })
            .fail(function(error) {
                $("#common-error").html("<p>Import File Fail!</p>");
                $("#common-error")
                    .removeClass("d-none")
                    .addClass("d-block");
            });
    });

    $(document).on("change", "#city", function(e) {
        let cityId = $(this).val();

        getDistrictList(cityId);
    });

    $(document).on("click", "#import-save", function(e) {
        e.preventDefault();

        Swal.fire({
            title: "Save Imported Profiles!",
            html: "Please Wating", // add html attribute if you want or remove
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
            onRender: () => {
                $.ajax({
                    url: rootUrl + "/profiles/import-save",
                    method: "POST",
                    data: {
                        profile_list: window.profileList,
                        city_id: $("#city").val(),
                        district_id: $("#district").val()
                    }
                })
                    .done(function(data) {
                        if (data.success) {
                            $("#common-error")
                                .removeClass("d-block")
                                .addClass("d-none");
                            $("#file-content").html("");

                            Swal.fire({
                                title: "Import Profiles",
                                text: `Import ${window.profileList.length} profiles successfully!`,
                                icon: "success"
                            });
                        } else {
                            Swal.fire({
                                title: "Import Profiles",
                                text: `Import ${window.profileList.length} profiles failed!`,
                                icon: "error"
                            });
                        }
                    })
                    .fail(function(error) {
                        console.log(error);
                        Swal.fire({
                            title: "Import Profiles",
                            text: `Import profiles failed!`,
                            icon: "error"
                        });
                    })
                    .always(function() {
                        Swal.hideLoading();
                        $("#loading")
                            .removeClass("d-block")
                            .addClass("d-none");
                    });
            }
        });
    });
});

function getDistrictList(cityId) {
    let currentValue = $("#district").data("value");
    $.ajax({
        url: rootUrl + `/district/get-from-city/${cityId}`,
        method: "GET"
    })
        .done(function(data) {
            let optionHTML = data.district_list.reduce((html, item) => {
                return (
                    html +
                    `<option value="${item.id}" ${
                        currentValue == item.id ? "selected" : ""
                    }>${item.display_name}</option>`
                );
            }, '<option value="0">Lựa chọn Quận/Huyện/Thị xã/TP</option>');

            $("#district").html(optionHTML);
            $("#district")
                .select2("destroy")
                .select2({
                    theme: "bootstrap4"
                });
        })
        .fail(function(error) {
            $("#district")
                .select2("destroy")
                .select2({
                    theme: "bootstrap4"
                });
        });
}
