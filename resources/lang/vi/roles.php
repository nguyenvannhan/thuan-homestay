<?php

return [
    'display_name' => [
        'admin' => 'Tất cả quyền',
        'users' => 'Quản lý users',
        'profiles' => 'Quản lý profiles',
        'accountants' => 'Quản lý thu - chi'
    ]
];
