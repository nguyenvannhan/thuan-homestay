<?php
return [
    'display_name' => [
        'users_list' => 'Xem danh sách users',
        'users_create' => 'Tạo mới user',
        'users_update' => 'Update thông tin user',
        'users_reset_password' => 'Reset Password',
        'users_change_status' => 'Đổi trạng thái user',

        'profiles_create' => 'Tạo mới profile',
        'profiles_update' => 'Chỉnh sửa thông tin profile',
        'profiles_import' => 'Nhập danh sách profiles từ file',

        'accountants_list' => 'Xem danh sách thu chi',
        'accountants_create' => 'Tạo mới khoản thu - chi',
        'accountants_update' => 'Chỉnh sửa thông tin thu - chi',
    ]
];
