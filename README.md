# **PROFILE MARKETING MANAGEMENT**

This system manage the profiles which is result of marketing campaign.
This include 4 roles: 

- This system have 4 Roles: **Admin**, **Accountant**, **Marketer**, **TeleSale**

This is roles and permissions list in System:

|                 |        |        Admin       | Marketer | TeleSale | Accountant |
| ----------------- | -------- | -------------------- | --- | --- | --- |
| User Management | Create |  :white_check_mark: |  :x: | :x: | :x: |
|                 | Update Info |  :white_check_mark: | :x: |  :x: | :x: |
|                 | Reset Password For User |  :white_check_mark: |  :x: | :x: | :x: |
| Profile Management | Create |  :white_check_mark: |  :white_check_mark: | :x: | :x: |
|                 | Update Info |  :white_check_mark: | :x: |  :white_check_mark: | :x: |
|                 | Import From File |  :white_check_mark: |  :white_check_mark: | :x: | :x: |
| Income-Outcome Management | Add Income-Outcome |  :white_check_mark: |  :x: | :x: | :white_check_mark: |
| Searching and See Report Info | | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |

This project use: 
- [Laravel Framwork 7.x](https://laravel.com/)
- [Admin LTE 3.x](https://adminlte.io/themes/v3/)

Some images about project: 

![Login Page](public/images_md/login.png "Login Page")
![Dashboard Page](public/images_md/dashboard.png "Dashboard Page")
![Users Page](public/images_md/users.png "Users Page")
![Profile Page](public/images_md/profiles.png "Profile Page")
![Import From File Page](public/images_md/import.png "Import From File Page")
![Income - Outcome Page](public/images_md/imcome_outcome.png "Income - Outcome Page")



