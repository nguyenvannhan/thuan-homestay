<?php

if (!function_exists('phone_str_to_array')) {
    function phone_str_to_array($phoneStr)
    {
        $phone = str_replace([' ', '_', '-'], '', $phoneStr);
        $phoneList = preg_split('/(;|,)/', $phone);

        return $phoneList;
    }
}

if (!function_exists('standardize_profile_phone')) {
    function standardize_profile_phone($phoneStr)
    {
        $phoneList = phone_str_to_array($phoneStr);

        return implode(', ', $phoneList);
    }
}
