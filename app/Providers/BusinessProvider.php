<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BusinessProvider extends ServiceProvider
{
    public $singletons = [
        \App\Core\Business\Contracts\AccountantInterface::class => \App\Core\Business\AccountantBusiness::class,
        \App\Core\Business\Contracts\CityInterface::class => \App\Core\Business\CityBusiness::class,
        \App\Core\Business\Contracts\DistrictInterface::class => \App\Core\Business\DistrictBusiness::class,
        \App\Core\Business\Contracts\HomeInterface::class => \App\Core\Business\HomeBusiness::class,
        \App\Core\Business\Contracts\ProfileInterface::class => \App\Core\Business\ProfileBusiness::class,
        \App\Core\Business\Contracts\UserInterface::class => \App\Core\Business\UserBusiness::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
