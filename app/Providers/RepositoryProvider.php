<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    public $singletons = [
        \App\Core\Repository\Contracts\AccountantInterface::class => \App\Core\Repository\AccountantRepository::class,
        \App\Core\Repository\Contracts\CityInterface::class => \App\Core\Repository\CityRepository::class,
        \App\Core\Repository\Contracts\DistrictInterface::class => \App\Core\Repository\DistrictRepository::class,
        \App\Core\Repository\Contracts\PermissionInterface::class => \App\Core\Repository\PermissionRepository::class,
        \App\Core\Repository\Contracts\ProfileInterface::class => \App\Core\Repository\ProfileRepository::class,
        \App\Core\Repository\Contracts\ProfileStatusHistoryInterface::class => \App\Core\Repository\ProfileStatusHistoryRepository::class,
        \App\Core\Repository\Contracts\RoleInterface::class => \App\Core\Repository\RoleRepository::class,
        \App\Core\Repository\Contracts\UserInterface::class => \App\Core\Repository\UserRepository::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
