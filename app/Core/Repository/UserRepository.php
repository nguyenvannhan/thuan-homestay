<?php

namespace App\Core\Repository;

use App\Core\Repository\Contracts\PermissionInterface as PermissionRepository;
use App\Core\Repository\Contracts\UserInterface;

class UserRepository extends BaseHelper implements UserInterface
{
    protected $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        parent::__construct();

        $this->permissionRepository = $permissionRepository;
    }

    public function getModel()
    {
        return \App\Models\User::class;
    }

    public function getListByPermission($permission, $options = [])
    {
        if (!\is_string($permission)) {
            $permission = $this->permissionRepository->findByIds([$permission]);
        }

        $query = $this->getModelToQuery()->permission($permission);

        return $this->buildQuery($query, $options);
    }

    public function countUser($fromDate, $toDate, $roleList = [])
    {
        return $this->getModelToQuery()->when(!empty($roleList), function ($roleQuery) use ($roleList) {
            $roleQuery->whereHas('roles', function ($roleTableQuery) use ($roleList) {
                $roleTableQuery->whereIn('name', $roleList);
            });
        }, function ($noneRoleQuery) {
            $noneRoleQuery->whereHas('roles', function ($roleTableQuery) {
                $roleTableQuery->where('name', '<>', 'admin');
            });
        })->whereBetween('created_at', [$fromDate, $toDate])->count();
    }
}
