<?php

namespace App\Core\Repository;

use App\Core\Repository\Contracts\PermissionInterface;

class PermissionRepository extends BaseHelper implements PermissionInterface
{
    public function getModel()
    {
        return \App\Models\Permission::class;
    }

    public function findByIds($ids, $options = [])
    {
        $query = $this->getModelToQuery(false)->whereIn('id', $ids);

        return $this->buildQuery($query, $options);
    }
}
