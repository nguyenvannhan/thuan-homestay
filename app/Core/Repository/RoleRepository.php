<?php

namespace App\Core\Repository;

use App\Core\Repository\Contracts\RoleInterface;

class RoleRepository extends BaseHelper implements RoleInterface
{
    public function getModel()
    {
        return \App\Models\Role::class;
    }

    public function setCommonQueries($query)
    {
        return $query->where('name', '<>', 'admin');
    }
}
