<?php

namespace App\Core\Repository;

use App\Core\Repository\Contracts\ProfileStatusHistoryInterface;
use App\Models\ProfileStatusHistory;

class ProfileStatusHistoryRepository extends BaseHelper implements ProfileStatusHistoryInterface

{
    public function getModel()
    {
        return ProfileStatusHistory::class;
    }


    public function countProfile($fromDate, $toDate, int $statusId = -1)
    {
        return $this->getModelToQuery()->has('profile')->when($statusId !== -1, function ($statusQuery) use ($statusId) {
            $statusQuery->where('status_id', $statusId);
        })->whereBetween('created_at', [$fromDate, $toDate])->distinct('profile_id')->count();
    }
}
