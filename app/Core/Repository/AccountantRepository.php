<?php

namespace App\Core\Repository;

use App\Core\Repository\Contracts\AccountantInterface;

class AccountantRepository extends BaseHelper implements AccountantInterface
{
    public function getModel()
    {
        return \App\Models\Accountant::class;
    }
}
