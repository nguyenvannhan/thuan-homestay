<?php

namespace App\Core\Repository;

use App\Core\Repository\Contracts\CityInterface;
use App\Models\City;

class CityRepository extends BaseHelper implements CityInterface
{

    public function getModel()
    {
        return City::class;
    }
}
