<?php

namespace App\Core\Repository;

use App\Core\Repository\Contracts\ProfileInterface;
use App\Models\Profile;

class ProfileRepository extends BaseHelper implements ProfileInterface
{
    public function getModel()
    {
        return Profile::class;
    }

    public function getListWithStatus($status, $withTrashed, $options = [])
    {
        $query = $this->getModelToQuery($withTrashed)->when($status !== -1, function ($statusQuery) use ($status) {
            $statusQuery->where('status', $status);
        });

        return $this->buildQuery($query, $options);
    }

    public function checkPhoneListExisted($phone, $except_ids = [])
    {
        $query = $this->getModelToQuery(true)->filterPhone($phone)->when(!empty($except_ids), function ($exceptQuery) use ($except_ids) {
            $exceptQuery->whereNotIn('id', $except_ids);
        });

        return $this->buildQuery($query, [
            'checkExists' => true,
        ]);
    }
}
