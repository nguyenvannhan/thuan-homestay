<?php
namespace App\Core\Repository;


use App\Core\Repository\Contracts\DistrictInterface;
use App\Models\District;

class DistrictRepository extends BaseHelper implements DistrictInterface
{

    public function getModel()
    {
        return District::class;
    }

    public function getListByCityId(int $cityId)
    {
        $query = $this->getModelToQuery()->where('city_id', $cityId);

        return $this->buildQuery($query, [
            'orders' => [
                ['name', 'ASC']
            ]
        ]);
    }
}
