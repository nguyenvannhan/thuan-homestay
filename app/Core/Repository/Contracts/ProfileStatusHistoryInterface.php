<?php

namespace App\Core\Repository\Contracts;

interface ProfileStatusHistoryInterface extends BaseInterface
{
    public function countProfile($fromDate, $toDate, int $statusId = -1);
}
