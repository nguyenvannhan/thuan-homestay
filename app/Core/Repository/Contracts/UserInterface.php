<?php

namespace App\Core\Repository\Contracts;

interface UserInterface extends BaseInterface
{
    /**
     * Get User List By Permission
     *
     * @param mixed $permission : Type value
     * @param array $options
     *
     * @return Collection
     *
     */
    public function getListByPermission($permission, $options = []);

    public function countUser($fromDate, $toDate, $roleId = 0);
}
