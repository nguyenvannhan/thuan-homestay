<?php


namespace App\Core\Repository\Contracts;


interface DistrictInterface extends BaseInterface
{
    /**
     * Get District List By City Id
     *
     * @param int $cityId
     *
     * @return array
     */
    public function getListByCityId(int $cityId);
}
