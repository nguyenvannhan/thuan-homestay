<?php

namespace App\Core\Repository\Contracts;

interface ProfileInterface extends BaseInterface
{
    public function getListWithStatus($status, $withTrashed, $options = []);

    public function checkPhoneListExisted(array $phoneList, $except_ids = []);
}
