<?php

namespace App\Core\Repository\Contracts;

interface PermissionInterface extends BaseInterface
{
    public function findByIds($ids, $options = []);
}
