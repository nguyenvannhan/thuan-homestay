<?php

namespace App\Core\Business;

use App\Core\Business\Contracts\CURDInterface;
use App\Core\Business\Contracts\DataTableInterface;
use App\Core\Business\Contracts\ProfileInterface;
use App\Core\Business\Traits\ProfileImportTrait;
use App\Core\Repository\Contracts\CityInterface as CityRepository;
use App\Core\Repository\Contracts\DistrictInterface as DistrictRepository;
use App\Core\Repository\Contracts\ProfileInterface as ProfileRepository;
use App\Core\Repository\Contracts\ProfileStatusHistoryInterface as ProfileStatusRepository;
use App\Core\Repository\Contracts\UserInterface as UserRepository;
use App\Models\Accountant;
use App\Models\Profile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class ProfileBusiness implements ProfileInterface, CURDInterface, DataTableInterface
{
    use ProfileImportTrait;

    protected $cityRepository;
    protected $districtRepository;
    protected $profileRepository;
    protected $profileStatusRepository;
    protected $userRepository;

    public function __construct(CityRepository $cityRepository, DistrictRepository $districtRepository, ProfileRepository $profileRepository, ProfileStatusRepository $profileStatusRepository, UserRepository $userRepository)
    {
        $this->cityRepository = $cityRepository;
        $this->districtRepository = $districtRepository;
        $this->profileRepository = $profileRepository;
        $this->profileStatusRepository = $profileStatusRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $marketerList = $this->userRepository->getListByPermission(Profile::PERMISSION_CREATE, [
            'columns' => ['id', 'name']
        ]);

        return [
            'marketerList' => $marketerList
        ];
    }

    public function create()
    {
        $cityList = $this->cityRepository->getList(['id', 'name', 'prefix']);

        return [
            'cityList' => $cityList,
        ];
    }

    public function store($data)
    {
        $data['phone'] = \standardize_profile_phone($data['phone']);

        $profile = $this->profileRepository->create($data);

        $statusData = [
            'profile_id' => $profile->id,
            'status_id' => $data['status_id'] ?? Profile::DANG_KY_MOI
        ];

        return $this->profileStatusRepository->create($statusData);
    }

    public function edit($id)
    {
        $profile = $this->profileRepository->findOrFail($id, false, ['district']);
        $cityList = $this->cityRepository->getList(['id', 'name', 'prefix']);
        if ($profile->district) {
            $districtList = $this->districtRepository->getListByCityId($profile->district->city_id);
        } else {
            $districtList = collect([]);
        }

        return [
            'profile' => $profile,
            'cityList' => $cityList,
            'districtList' => $districtList,
        ];
    }

    public function update($id, $data)
    {
        $profile = $this->profileRepository->findOrFail($id);
        DB::beginTransaction();

        try {
            if ($profile->status_id != $data['status_id']) {
                if ($data['status_id'] == Profile::DA_DANG_KY) {
                    $depositFee = $profile->deposited_fee;

                    if ($depositFee) {
                        $depositFee->update([
                            'explain' => $depositFee->explain . '<br><p>Đặt cọc chuyển thành phí vì thay đổi trạng thái</p>',
                            'type_id' => Accountant::INCOME_ID,
                        ]);
                    }
                }

                $this->profileStatusRepository->create([
                    'profile_id' => $profile->id,
                    'status_id' => $data['status_id']
                ]);
            }
            $profile->update($data);

            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage(), [
                $id, $data
            ]);

            return false;
        }
    }

    public function show($id)
    {
        return $this->profileRepository->findOrFail($id);
    }

    public function destroy($id)
    {
        $profile = $this->profileRepository->findOrFail($id);

        return $profile->delete();
    }

    public function getTableData(Request $request)
    {
        $profile = $this->profileRepository->getModelToQuery()->with(['district.city', 'marketer', 'tele_sale', 'paid_fee', 'deposited_fee', 'city']);

        return DataTables::of($profile)
            ->filter(function ($query) use ($request) {
                if (!is_null($request->status)) {
                    $query->where('status_id', $request->status);
                }

                if ($request->marketer_id) {
                    $query->where('marketer_id', $request->marketer_id);
                }

                if ($request->phone) {
                    $query->filterPhone($request->phone);
                }

                $fromDate = $request->from_date ? Carbon::createFromFormat('d/m/Y', $request->from_date)->startOfDay()->format('Y-m-d  H:i:s') : null;
                $toDate = $request->to_date ? Carbon::createFromFormat('d/m/Y', $request->to_date)->endOfDay()->format('Y-m-d H:i:s') : null;

                $query->filterDate($fromDate, $toDate);
            })
            ->addColumn('action', function ($profile) {
                $content = '';
                if (Auth::user()->hasPermissionTo(Profile::PERMISSION_UPDATE)) {
                    $content .= '<div class="text-center mb-2"><a href="#" data-id="' . $profile->id . '" class="btn btn-xs btn-primary btn-edit mx-1"><i class="fa fa-edit mr-1"></i> Edit</a></div>';
                }

                if (Auth::user()->hasPermissionTo(Accountant::ACCOUNTANT_CREATE)) {
                    if ($profile->isRegistered()) {
                        $content .= '<div class="text-center mb-2"><a href="#" data-id="' . $profile->id . '" class="btn btn-xs btn-success btn-fee mx-1"><i class="fas fa-money-bill-wave mr-1"></i> Fee</a></div>';
                    }

                    if ($profile->isDeposited() && !$profile->deposited_fee) {
                        $content .= '<div class="text-center mb-2"><a href="#" data-id="' . $profile->id . '" class="btn btn-xs btn-info btn-deposit mx-1"><i class="fas fa-wallet mr-1"></i> Đặt cọc</a></div>';
                    }
                }

                if (Auth::user()->hasRole('admin')) {
                    $content .= '<div class="text-center mb-2"><a href="#" data-id="' . $profile->id . '" class="btn btn-xs btn-danger btn-delete mx-1"><i class="fas fa-trash mr-1"></i> Xoá</a></div>';
                }

                return $content;
            })
            ->editColumn('profile', function ($profile) {
                return $profile->profile_info;
            })
            ->editColumn('author', function ($profile) {
                return '<span class="text-info">Marketer: <strong>' . $profile->marketer->name . '</strong></span><br>' .
                '<span class="text-primary">Tele: <strong>' . ($profile->tele_sale ? $profile->tele_sale->name : '') . '</strong></span>';
            })
            ->editColumn('status', function ($profile) {
                return '<span class="badge badge-' . $profile->status_id . '">' . Profile::STATUS_LIST[$profile->status_id] . '</span>';
            })
            ->removeColumn('name', 'email', 'phone', 'created_at', 'updated_at', 'district_id', 'district', 'marketer_id', 'tele_sale_id', 'marketer', 'tele_sale', 'address', 'status_id', 'deleted_at')
            ->rawColumns(['profile', 'action', 'note', 'author', 'status'])
            ->make(true);
    }
}
