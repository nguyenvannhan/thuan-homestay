<?php

namespace App\Core\Business;

use App\Core\Business\Contracts\AccountantInterface;
use App\Core\Business\Contracts\CURDInterface;
use App\Core\Repository\Contracts\AccountantInterface as AccountantRepository;
use App\Core\Repository\Contracts\ProfileInterface as ProfileRepository;
use App\Models\Accountant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class AccountantBusiness implements AccountantInterface, CURDInterface
{
    protected $accountantRepository;
    protected $profileRepository;

    public function __construct(AccountantRepository $accountantRepository, ProfileRepository $profileRepository)
    {
        $this->accountantRepository = $accountantRepository;
        $this->profileRepository = $profileRepository;
    }

    public function index()
    {
        $request = Request();

        $fromDate = $request->from_date ? Carbon::createFromFormat('d/m/Y', $request->from_date)->format('d/m/Y') : Carbon::now()->firstOfMonth()->format('d/m/Y');
        $toDate = $request->to_date ? Carbon::createFromFormat('d/m/Y', $request->to_date)->format('d/m/Y') : Carbon::today()->format('d/m/Y');

        return [
            'fromDate' => $fromDate,
            'toDate' => $toDate,
        ];
    }

    public function create()
    {
        return null;
    }

    public function store($data)
    {
        if ($this->accountantRepository->create($data)) {
            return [
                'success' => true,
            ];
        }

        return [
            'success' => false,
            'message' => 'Tạo thu chi thất bại!',
        ];
    }

    public function edit($id)
    {
        return [
            'accountantItem' => $this->accountantRepository->findOrFail($id)
        ];
    }

    public function update($id, $data)
    {
        $accountant = $this->accountantRepository->findOrFail($id);

        if ($accountant->update($data)) {
            return [
                'success' => true,
            ];
        }

        return [
            'success' => false,
            'message' => 'Update thu chi thất bại!',
        ];
    }

    public function destroy($id)
    {
        $accountant = $this->accountantRepository->findOrFail($id);

        return $accountant->delete();
    }

    public function getTableData($type)
    {
        $accountantQuery = $this->accountantRepository->getModelToQuery()->with(['profile' => function ($profileQuery) {
            $profileQuery->withTrashed();
        }, 'user']);

        if ($type === Accountant::INCOME_ID) {
            $accountantQuery->onlyIncome();
        } else {
            $accountantQuery->onlyOutcome();
        }

        return DataTables::of($accountantQuery)
            ->filter(function ($query) {
                $fromDate = Carbon::createFromFormat('d/m/Y', Request()->from_date)->startOfDay()->format('Y-m-d H:i:s');
                $toDate = Carbon::createFromFormat('d/m/Y', Request()->to_date)->endOfDay()->format('Y-m-d H:i:s');

                $query->whereBetween('created_at', [$fromDate, $toDate]);
            })
            ->editColumn('money', function ($accountant) {
                return number_format($accountant->money, 0, '.', ',') . ' VNĐ';
            })
            ->editColumn('content', function ($accountant) {
                $contentHtml = '<p class="mb-0">';
                $contentHtml .= $accountant->explain;
                if ($accountant->profile) {
                    $contentHtml .= '<br>Profile: <strong>' . ($accountant->profile ? ($accountant->profile->name . ($accountant->profile->trashed() ? '<span class="text-danger mx-1 font-italic">(Deleted)</span>' : '')) : '') . '</strong><br>';
                }
                $contentHtml .=
                'User created: <strong>' . ($accountant->user ? $accountant->user->name : '') . '</strong>';
                $contentHtml .= '</p>';

                return $contentHtml;
            })
            ->editColumn('action', function ($accountant) {
                $contentHtml = '';

                if (Auth::user()->hasPermissionTo('accountants_update')) {
                    $contentHtml .= '<div class="text-center mb-2"><a href="' . route('accountant.edit', $accountant->id) . '" class="btn btn-xs btn-primary btn-edit mx-1"><i class="fa fa-edit mr-1"></i> Edit</a></div>';
                }

                if (Auth::user()->hasRole('admin')) {
                    $contentHtml .= '<div class="text-center mb-2"><a href="#" data-id="' . $accountant->id . '" class="btn btn-xs btn-danger btn-delete mx-1"><i class="fas fa-trash mr-1"></i> Xoá</a></div>';
                }

                return $contentHtml;
            })
            ->removeColumn('profile_id', 'type_id', 'user_id', 'explain', 'updated_at', 'deleted_at')
            ->rawColumns(['content', 'action'])
            ->make(true);
    }

    public function getProfileFeeDetail($profileId)
    {
        $profile = $this->profileRepository->findOrFail($profileId, false, ['paid_fee'], ['id', 'name']);

        return [
            'success' => true,
            'view' => view('accountant.profile')->with([
                'profile' => $profile,
                'feeList' => $profile->paid_fee
            ])->render()
        ];
    }

    public function getProfileDepositDetail($accountantId, $profileId)
    {
        $profile = $this->profileRepository->findOrFail($profileId, false, ['deposited_fee'], ['id', 'name']);

        if ($profile->deposited_fee->id != $accountantId) {
            abort(500);
        }

        return $profile->deposited_fee->only(['id', 'explain', 'money', 'profile_id']);
    }

    public function updateProfileDepositDetail($accountantId, $data)
    {
        if ($accountantId == 0) {
            return $this->store(
                array_merge($data, [
                    'type_id' => Accountant::DEPOSIT_ID,
                ])
            );
        } else {
            $accountant = $this->accountantRepository->findOrFail($accountantId);

            if (! isset($data['profile_id']) || $accountant->profile_id != $data['profile_id']) {
                abort(500);
            }

            return $accountant->update($data);
        }
    }

    public function getSum($fromDate, $toDate)
    {
        $accountantList = $this->accountantRepository->getModelToQuery()->whereBetween('created_at', [$fromDate, $toDate]);

        $accountantList = $accountantList->get(['id', 'money', 'type_id']);

        return [
            'success' => true,
            'sumOfIncome' => number_format($accountantList->whereIn('type_id', [Accountant::INCOME_ID, Accountant::DEPOSIT_ID])->sum('money'), 0, '.', ',') . ' VNĐ',
            'sumOfOutcome' => number_format($accountantList->where('type_id', Accountant::OUTCOME_ID)->sum('money'), 0, '.', ',') . ' VNĐ'
        ];
    }
}
