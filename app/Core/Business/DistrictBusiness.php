<?php

namespace App\Core\Business;

use App\Core\Business\Contracts\DistrictInterface;
use App\Core\Repository\Contracts\DistrictInterface as DistrictRepository;

class DistrictBusiness implements DistrictInterface
{
    protected $districtRepository;

    public function __construct(DistrictRepository $districtRepository)
    {
        $this->districtRepository = $districtRepository;
    }

    public function getListByCityId(int $cityId)
    {
        return $this->districtRepository->getListByCityId($cityId);
    }
}
