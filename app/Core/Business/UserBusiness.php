<?php

namespace App\Core\Business;

use App\Core\Business\Contracts\DataTableInterface;
use App\Core\Business\Contracts\UserInterface;
use App\Core\Repository\Contracts\PermissionInterface as PermissionRepository;
use App\Core\Repository\Contracts\RoleInterface as RoleRepository;
use App\Core\Repository\Contracts\UserInterface as UserRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class UserBusiness implements UserInterface, DataTableInterface
{
    protected $permissionRepository;
    protected $roleRepository;
    protected $userRepository;

    public function __construct(PermissionRepository $permissionRepository, RoleRepository $roleRepository, UserRepository $userRepository)
    {
        $this->permissionRepository = $permissionRepository;
        $this->roleRepository = $roleRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        return [
            'roleList' => $this->roleRepository->getList(['*'], [
                'with' => 'permissions'
            ]),
        ];
    }

    public function create()
    {
        return [
            'roleList' => $this->roleRepository->getList(['*'], [
                'with' => 'permissions'
            ])
        ];
    }

    public function store($data)
    {
        $permissionIds = $data['permission_ids'];

        unset($data['permission_ids']);

        list($roles, $permissions) = $this->analysisRoleAndPermissions($permissionIds);

        DB::beginTransaction();

        try {
            $user = $this->userRepository->create($data);

            if (count($roles)) {
                $user->syncRoles($roles);
            }
            if (count($permissions)) {
                $user->syncPermissions($permissions);
            }

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollback();

            return false;
        }
    }

    public function edit($id)
    {
        $userItem = $this->userRepository->findOrFail($id);

        return [
            'roleList' => $this->roleRepository->getList(['*'], [
                'with' => 'permissions'
            ]),
            'userItem' => $userItem
        ];
    }

    public function update($id, $data)
    {
        $permissionIds = $data['permission_ids'];

        unset($data['permission_ids']);

        list($roles, $permissions) = $this->analysisRoleAndPermissions($permissionIds);

        DB::beginTransaction();

        try {
            $user = $this->userRepository->findOrFail($id);

            $user->update($data);

            if (count($roles)) {
                $user->syncRoles($roles);
            }
            if (count($permissions)) {
                $user->syncPermissions($permissions);
            }

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollback();

            return false;
        }
    }

    private function analysisRoleAndPermissions($permissionIds)
    {
        $roleList = $this->roleRepository->getList(['*'], [
            'with' => ['permissions']
        ]);

        $officialRoleIds = [];
        $officialPermissionIds = [];

        foreach ($roleList as $role) {
            $rolePermissionIds = $role->permissions->pluck('id');

            $filteredPermissionIds = collect($permissionIds)->intersect($rolePermissionIds->toArray());

            if ($rolePermissionIds->count() === $filteredPermissionIds->count()) {
                $officialRoleIds[] = $role->id;
            } else {
                $officialPermissionIds[] = $filteredPermissionIds;
            }

            $permissionIds = collect($permissionIds)->diff($filteredPermissionIds->toArray())->toArray();
        }

        return [
            Arr::flatten($officialRoleIds),
            Arr::flatten($officialPermissionIds)
        ];
    }

    public function changeStatus($id)
    {
        $user = $this->userRepository->findOrFail($id, true, [], ['id', 'deleted_at']);

        if ($user->trashed()) {
            $user->restore();
        } else {
            $user->delete();
        }

        return !$user->trashed();
    }

    public function resetPassword($id)
    {
        $user = $this->userRepository->findOrFail($id, false, [], ['id', 'password']);

        $newPassword = Str::random(10);

        if ($user->update(['password' => $newPassword])) {
            return [
                'success' => true,
                'password' => $newPassword,
            ];
        }

        return [
            'success' => false
        ];
    }

    public function getTableData(Request $request)
    {
        $userList = $this->userRepository->getModelToQuery(true)->where('id', '<>', Auth::id())->excludeAdmin()->select('id', 'email', 'name', 'created_at', 'updated_at', 'deleted_at');

        return DataTables::of($userList)
        ->filter(function ($query) use ($request) {
            if ($request->search_keyword) {
                $query->where('name', 'LIKE', '%' . $request->search_keyword . '%')->orWhere('email', 'LIKE', '%' . $request->search_keyword . '%');
            }

            if ($request->permission_ids) {
                $permissions = $this->permissionRepository->findByIds($request->permission_ids);

                $query->permission($permissions);
            }
        })
        ->editColumn('role', function ($user) {
            $roleContent = '';
            foreach ($user->roles as $roleItem) {
                $roleContent .= '<span class="badge badge-success mx-1 px-2 px-1">' . ucwords(trans('roles.display_name.' . $roleItem->name)) . '</span>';
            }
            foreach ($user->permissions as $permissionItem) {
                $roleContent .= '<span class="badge badge-warning mx-1 px-2 px-1">' . ucwords(trans('permissions.display_name.' . $permissionItem->name)) . '</span>';
            }

            return $roleContent;
        })
        ->editColumn('timestamps', function ($user) {
            $timestampsContent = 'Created at: <strong class="text-primary">' . $user->created_at . '</strong><br>' .
            'Last Updated at: <strong class="text-warning">' . $user->updated_at . '</strong><br>';

            if ($user->trashed()) {
                $timestampsContent .= 'Disabled at: <strong class="text-danger">' . $user->deleted_at . '</strong>';
            }

            return $timestampsContent;
        })
        ->editColumn('status', function ($user) {
            if ($user->trashed()) {
                return '<button class="btn btn-danger ' . (Auth::user()->hasPermissionTo('users_change_status') ? 'btn-status' : '') . ' btn-sm" data-id="' . $user->id . '">Disabled</button>';
            }

            return '<button class="btn btn-success ' . (Auth::user()->hasPermissionTo('users_change_status') ? 'btn-status' : '') . ' btn-sm" data-id="' . $user->id . '">Active</button>';
        })
        ->editColumn('action', function ($user) {
            if ($user->trashed()) {
                return '';
            }
            $actionHtml = '';
            if (Auth::user()->hasPermissionTo('users_reset_password')) {
                $actionHtml .= '<button class="btn btn-link btn-reset-pass" data-id="' . $user->id . '">Reset Password</button>';
            }
            if (Auth::user()->hasPermissionTo('users_update')) {
                $actionHtml .= '<a class="" data-id="' . $user->id . '" href="' . route('user.edit', $user->id) . '">Update User</a>';
            }
            return $actionHtml;
        })
        ->removeColumn('created_at', 'updated_at', 'deleted_at')
        ->rawColumns(['role', 'timestamps', 'status', 'action'])
        ->make(true);
    }
}
