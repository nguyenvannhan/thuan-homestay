<?php

namespace App\Core\Business\Traits;

use App\Core\Repository\Contracts\CityInterface;
use App\Core\Repository\Contracts\DistrictInterface;
use App\Core\Repository\Contracts\ProfileInterface;
use App\Imports\ProfilesImport;
use App\Models\Profile;
use Exception;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

trait ProfileImportTrait
{
    public function import($file)
    {
        try {
            list($profileList, $emailList, $phoneList) = $this->readFile($file);

            $emailErrors = $this->getEmailErrorsList($emailList);

            $phoneErrors = $this->getPhoneErrorsList($phoneList);

            $profileList = $this->standardizeProfileList($profileList, $emailErrors, $phoneErrors);

            if (!count($phoneErrors) && !count($emailErrors)) {
                $cityList = resolve(CityInterface::class)->getList(['id', 'name']);
            } else {
                $cityList = collect([]);
            }

            $html = view('profile.import.file-content')->with([
                'profileList' => $profileList,
                'emailErrors' => $emailErrors,
                'phoneErrors' => $phoneErrors,
                'cityList' => $cityList,
            ])->render();

            return [
                'success' => true,
                'profileList' => $profileList,
                'html' => $html
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => 'Nội dung file không đúng!',
                $e->getMessage()
            ];
        }
    }

    public function saveImport($profileList, $city_id, $district_id)
    {
        $district = resolve(DistrictInterface::class)->find($district_id, false, [], ['id', 'name', 'city_id']);

        if (!is_null($district) && $city_id != 0 && $district->city_id != $city_id) {
            return [
                'success' => false,
                'message' => 'Giá trị Tỉnh/Thành phố và Quận/Huyện không khớp',
            ];
        }

        $profileList = collect($profileList)->map(function ($item) use ($city_id, $district_id) {
            $item['phone'] = standardize_profile_phone($item['phone']);
            $item['district_id'] = $district_id;
            $item['city_id'] = $city_id;

            unset($item['phone_list'], $item['phone_html']);

            return $item;
        });

        DB::beginTransaction();

        try {
            foreach ($profileList as $profile) {
                resolve(ProfileInterface::class)->create($profile);
            }

            DB::commit();

            return[
                'success' => true,
            ];
        } catch (Exception $e) {
            DB::rollback();

            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }
    }

    private function readFile($file)
    {
        $profileCollections = Excel::toCollection(new ProfilesImport, $file);

        $profileList = [];
        $phoneList = [];
        $emailList = [];

        foreach ($profileCollections->first() as $key => $profileCollection) {
            if ($key !== 0) {
                $profile = new Profile;
                $profile->name = $profileCollection[1];
                $profile->email = $profileCollection[2];
                $profile->phone = $profileCollection[3];
                $profile->address = $profileCollection[4];
                $profile->note = $profileCollection[5];

                $profilePhoneList = $profile->phone_list;

                $profile->phone_list = $profilePhoneList;

                $emailList[] = $profile->email;
                $phoneList = array_merge($phoneList, $profilePhoneList);

                $profileList[] = $profile;
            }
        }

        return [$profileList, $emailList, $phoneList];
    }

    private function getEmailErrorsList($emailList)
    {
        $emailErrors = Profile::whereIn('email', $emailList)->get(['id', 'email']);
        $emailErrors = $emailErrors->map(function ($item) {
            return $item->email;
        })->toArray();

        return array_merge($emailErrors, $this->getSameValueElements($emailList));
    }

    private function getPhoneErrorsList($phoneList)
    {
        $phoneErrors = Profile::filterPhone($phoneList)->get(['id', 'phone']);
        $phoneErrors = $phoneErrors->map(function ($item) {
            return $item->phone_list;
        })->flatten()->toArray();

        return array_merge($this->getSameValueElements($phoneList), \array_intersect($phoneList, $phoneErrors));
    }

    public function standardizeProfileList($profileList, $emailErrors, $phoneErrors)
    {
        return collect($profileList)->map(function ($profile) use ($emailErrors, $phoneErrors) {
            if (in_array($profile->email, $emailErrors) !== false) {
                $profile->email_class = 'text-danger';
            }

            $phoneHtml = '';
            foreach ($profile->phone_list as $profilePhoneItem) {
                if (in_array($profilePhoneItem, $phoneErrors) !== false) {
                    $phoneHtml .= (empty($phoneHtml) ? '' : ',') . '<span class="text-danger">' . $profilePhoneItem . '</span>';
                } else {
                    $phoneHtml .= (empty($phoneHtml) ? '' : ',') . '<span>' . $profilePhoneItem . '</span>';
                }
            }
            $profile->phone_html = $phoneHtml;

            return $profile;
        });
    }

    private function getSameValueElements($array)
    {
        $errorValue = [];
        $uniqueElements = [];

        foreach ($array as $item) {
            if (in_array($item, $uniqueElements) !== false) {
                $errorValue[] = $item;
            } else {
                $uniqueElements[] = $item;
            }
        }

        return $errorValue;
    }
}
