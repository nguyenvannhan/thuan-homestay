<?php

namespace App\Core\Business;

use App\Core\Business\Contracts\HomeInterface;
use App\Core\Repository\Contracts\ProfileInterface as ProfileRepository;
use App\Core\Repository\Contracts\ProfileStatusHistoryInterface as ProfileStatusRepository;
use App\Core\Repository\Contracts\UserInterface as UserRepository;
use App\Models\Profile;

class HomeBusiness implements HomeInterface
{
    protected $profileRepository;
    protected $profileStatusRepository;
    protected $userRepository;

    public function __construct(ProfileRepository $profileRepository, ProfileStatusRepository $profileStatusRepository, UserRepository $userRepository)
    {
        $this->profileRepository = $profileRepository;
        $this->profileStatusRepository = $profileStatusRepository;
        $this->userRepository = $userRepository;
    }

    public function index($fromDate, $toDate)
    {
        $registeredProfileCount = $this->profileStatusRepository->countProfile($fromDate, $toDate, Profile::DA_DANG_KY);
        $totalProfileCount = $this->profileStatusRepository->countProfile($fromDate, $toDate);
        $userCount = $this->userRepository->countUser($fromDate, $toDate);

        return [
            'totalProfileCount' => $totalProfileCount,
            // 'newProfileCount' => $newProfileCount,
            'registeredProfileCount' => $registeredProfileCount,
            'userCount' => $userCount,
        ];
    }
}
