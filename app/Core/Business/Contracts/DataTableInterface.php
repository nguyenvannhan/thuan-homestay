<?php

namespace App\Core\Business\Contracts;

use Illuminate\Http\Request;

interface DataTableInterface
{
    public function getTableData(Request $request);
}
