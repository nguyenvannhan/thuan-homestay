<?php

namespace App\Core\Business\Contracts;

interface HomeInterface
{
    public function index($fromDate, $toDate);
}
