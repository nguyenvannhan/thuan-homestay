<?php
namespace App\Core\Business\Contracts;

use App\Models\City;

interface CityInterface {

    /**
     * @param $id
     *
     * @return City|null
     */
    public function show($id);
}
