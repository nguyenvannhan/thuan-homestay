<?php

namespace App\Core\Business\Contracts;

interface UserInterface
{
    public function index();

    public function create();

    public function store($data);

    public function edit($id);

    public function update($id, $data);

    public function changeStatus($id);

    public function resetPassword($id);
}
