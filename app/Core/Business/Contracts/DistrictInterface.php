<?php
namespace App\Core\Business\Contracts;

interface DistrictInterface {
    /**
     * Get District List By City Id
     *
     * @param int $cityId
     *
     * @return mixed
     */
    public function getListByCityId(int $cityId);
}
