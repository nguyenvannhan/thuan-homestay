<?php

namespace App\Core\Business\Contracts;

interface ProfileInterface
{
    public function import($file);
}
