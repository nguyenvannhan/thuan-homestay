<?php

namespace App\Core\Business\Contracts;

use Illuminate\Http\Request;

interface AccountantInterface
{
    // public function index(Request $request);

    // public function store($data);

    // public function edit($id);

    // public function update($id, $data);

    public function getTableData($type);

    public function getProfileFeeDetail($profileId);

    public function getProfileDepositDetail($accountantId, $profileId);

    public function updateProfileDepositDetail($accountantId, $data);

    public function getSum($fromDate, $toDate);
}
