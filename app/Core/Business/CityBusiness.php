<?php

namespace App\Core\Business;

use App\Core\Business\Contracts\CityInterface;
use App\Core\Repository\Contracts\CityInterface as CityRepository;

class CityBusiness implements CityInterface
{
    protected $cityRepository;

    public function __construct(CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    public function index()
    {
        return $this->cityRepository->getList();
    }

    public function show($id)
    {
        return $this->cityRepository->find($id, Request()->withTrashed);
    }
}
