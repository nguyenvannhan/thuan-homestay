<?php

namespace App\Http\Controllers;

use App\Core\Business\Contracts\DistrictInterface as DistrictBusiness;

class DistrictController extends Controller
{
    protected $districtBusiness;

    public function __construct(DistrictBusiness $districtBusiness)
    {
        $this->districtBusiness = $districtBusiness;
    }

    public function getFromCity($city_id)
    {
        $districtList = $this->districtBusiness->getListByCityId($city_id);

        $districtList = $districtList->map(function ($item) {
            $item->display_name = $item->display_name;
            return $item;
        });

        return response()->json(['district_list' => $districtList]);
    }
}
