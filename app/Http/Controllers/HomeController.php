<?php

namespace App\Http\Controllers;

use App\Core\Business\Contracts\HomeInterface as HomeBusiness;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $homeBusiness;

    public function __construct(HomeBusiness $homeBusiness)
    {
        $this->homeBusiness = $homeBusiness;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $fromDate = $request->from_date ? Carbon::createFromFormat('d/m/Y', $request->from_date) : Carbon::now()->firstOfMonth();
        $toDate = $request->to_date ? Carbon::createFromFormat('d/m/Y', $request->to_date) : Carbon::today();

        $data = $this->homeBusiness->index($fromDate->format('Y-m-d 00:00:00'), $toDate->format('Y-m-d 23:59:59'));

        return view('index')->with($data);
    }
}
