<?php

namespace App\Http\Controllers;

use App\Core\Business\Contracts\UserInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userBusiness;

    public function __construct(UserInterface $userBusiness)
    {
        $this->userBusiness = $userBusiness;
    }

    public function index()
    {
        $data = $this->userBusiness->index();

        return view('user.index')->with($data);
    }

    public function create()
    {
        $data = $this->userBusiness->create();

        return view('user.create')->with($data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'unique:users'
        ]);

        $data = $request->only(['name', 'email', 'password', 'permission_ids']);

        $result = $this->userBusiness->store($data);

        if ($result) {
            return redirect()->route('user.index')->with(['create_user' => true]);
        }

        return back()->withInput()->withErrors(['message' => 'Have error while creating User']);
    }

    public function edit($id)
    {
        $data = $this->userBusiness->edit($id);

        return view('user.edit')->with($data);
    }

    public function update(Request $request, $id)
    {
        $data = $request->only(['name', 'permission_ids']);

        $result = $this->userBusiness->update($id, $data);

        if ($result) {
            return redirect()->route('user.index')->with(['update_user' => true]);
        }

        return back()->withInput()->withErrors(['message' => 'Have error while updating User']);
    }

    public function resetPassword(Request $request)
    {
        $id = $request->id;

        $result = $this->userBusiness->resetPassword($id);

        return response()->json($result);
    }

    public function getDataTable(Request $request)
    {
        return $this->userBusiness->getTableData($request);
    }

    public function changeStatus(Request $request)
    {
        $status = $this->userBusiness->changeStatus($request->id);

        return response()->json(['status' => $status], 200);
    }
}
