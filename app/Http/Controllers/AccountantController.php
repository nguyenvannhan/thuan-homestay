<?php

namespace App\Http\Controllers;

use App\Core\Business\Contracts\AccountantInterface as AccountantBusiness;
use App\Models\Accountant;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AccountantController extends Controller
{
    protected $accountantBusiness;

    public function __construct(AccountantBusiness $accountantBusiness)
    {
        $this->accountantBusiness = $accountantBusiness;
    }

    public function index(Request $request)
    {
        $data = $this->accountantBusiness->index($request);

        return view('accountant.index')->with($data);
    }

    public function create()
    {
        return view('accountant.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'money' => 'required|numeric',
            'type_id' => 'required|in:' . implode(',', [Accountant::INCOME_ID, Accountant::OUTCOME_ID]),
            'explain' => 'required',
        ], [
            'money.required' => 'Vui lòng nhập số tiền.',
            'money.numeric' => 'Số tiền phải là số.',
            'type_id.required' => 'Vui lòng chọn loại',
            'type_id.in' => 'Giá trị loại không đúng!',
            'explain.required' => 'Vui lòng ghi chú nội dung.',
        ]);

        $result = $this->accountantBusiness->store($request->except(['_token']));

        if ($result['success']) {
            return redirect()->route('accountant.index')->with('create_accountant', true);
        }

        return back()->withInput()->withErrors(['message' => $result['message']]);
    }

    public function edit($id)
    {
        $data = $this->accountantBusiness->edit($id);

        return view('accountant.edit')->with($data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'money' => 'required|numeric',
            'type_id' => 'required|in:' . implode(',', [Accountant::INCOME_ID, Accountant::OUTCOME_ID]),
            'explain' => 'required',
        ], [
            'money.required' => 'Vui lòng nhập số tiền.',
            'money.numeric' => 'Số tiền phải là số.',
            'type_id.required' => 'Vui lòng chọn loại',
            'type_id.in' => 'Giá trị loại không đúng!',
            'explain.required' => 'Vui lòng ghi chú nội dung.',
        ]);

        $result = $this->accountantBusiness->update($id, $request->except(['_token']));

        if ($result['success']) {
            return redirect()->route('accountant.index')->with('update_accountant', true);
        }

        return back()->withInput()->withErrors(['message' => $result['message']]);
    }

    public function delete(Request $request)
    {
        return $this->accountantBusiness->destroy($request->id);
    }

    public function getIncomeDataTable()
    {
        return $this->accountantBusiness->getTableData(Accountant::INCOME_ID);
    }

    public function getOutcomeDataTable()
    {
        return $this->accountantBusiness->getTableData(Accountant::OUTCOME_ID);
    }

    public function profile($id)
    {
        return response()->json(
            $this->accountantBusiness->getProfileFeeDetail($id)
        );
    }

    public function getSum(Request $request)
    {
        $request->validate([
            'from_date' => 'required',
            'to_date' => 'required',
        ]);

        $fromDate = Carbon::createFromFormat('d/m/Y', Request()->from_date)->startOfDay()->format('Y-m-d H:i:s');
        $toDate = Carbon::createFromFormat('d/m/Y', Request()->to_date)->endOfDay()->format('Y-m-d H:i:s');

        return response()->json(
            $this->accountantBusiness->getSum($fromDate, $toDate)
        );
    }

    public function ajaxShowDeposit(Request $request)
    {
        return response()->json(
            $this->accountantBusiness->getProfileDepositDetail($request->accountant_id, $request->profile_id)
        );
    }

    public function ajaxUpdateDeposit(Request $request)
    {
        $request->validate([
            'profile_id' => 'required|numeric',
            'money' => 'required|numeric',
            'explain' => 'required'
        ]);

        return response()->json([
            $this->accountantBusiness->updateProfileDepositDetail($request->accountant_id ?? 0, $request->except(['_token', 'accountant_id']))
        ]);
    }
}
