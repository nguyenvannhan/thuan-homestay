<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function changePassword()
    {
        return view('auth.password');
    }

    public function postChangePassword(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'password' => 'required|confirmed',
        ]);

        if (!Hash::check($request->current_password, Auth::user()->password)) {
            return back()->withErrors(['Current Password is wrong!']);
        }

        $user = Auth::user();

        $user->password = $request->password;
        $user->save();

        Auth::logout();

        return redirect()->route('login');
    }
}
