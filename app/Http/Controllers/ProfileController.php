<?php

namespace App\Http\Controllers;

use App\Core\Business\Contracts\AccountantInterface as AccountantBusiness;
use App\Core\Business\Contracts\ProfileInterface as ProfileBusiness;
use App\Http\Requests\ProfileCreateRequest;
use App\Http\Requests\ProfileImportRequest;
use App\Http\Requests\ProfileUpdateRequest;
use App\Models\Accountant;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    protected $profileBusiness;
    protected $accountantBusiness;

    public function __construct(ProfileBusiness $profileBusiness, AccountantBusiness $accountantBusiness)
    {
        $this->profileBusiness = $profileBusiness;
        $this->accountantBusiness = $accountantBusiness;
    }

    public function index()
    {
        $data = $this->profileBusiness->index();

        return view('profile.index')->with($data);
    }

    public function edit($id)
    {
        $data = $this->profileBusiness->edit($id);

        return response()->json([
            'view' => view('profile.edit')->with($data)->render(),
        ]);
    }

    public function create()
    {
        $data = $this->profileBusiness->create();

        return view('profile.create')->with($data);
    }

    public function update(ProfileUpdateRequest $request, $id)
    {
        $data = $request->except(['_token']);

        if ($data['appointment_time']) {
            $data['appointment_time'] = Carbon::createFromFormat('d/m/Y H:i', $data['appointment_time'])->format('Y-m-d H:i:s');
        }

        if ($this->profileBusiness->update($id, $data)) {
            return response()->json(200);
        }

        return response()->json(['message' => 'Have errors'], 500);
    }

    public function store(ProfileCreateRequest $request)
    {
        $data = $request->only(['name', 'email', 'phone', 'district_id', 'address', 'note']);

        $result = $this->profileBusiness->store($data);

        if ($result) {
            return redirect()->route('profile.index')->with(['create_profile' => true]);
        }

        return back()->withInput()->withErrors(['message' => 'Have errors while creating new profile']);
    }

    public function show($id)
    {
        $profile = $this->profileBusiness->show($id);
    }

    public function delete(Request $request)
    {
        return $this->profileBusiness->destroy($request->id);
    }

    public function getDataTable(Request $request)
    {
        // return $request->all();
        return $this->profileBusiness->getTableData($request);
    }

    public function getImportFile()
    {
        return view('profile.import.index');
    }

    public function postImportFile(ProfileImportRequest $request)
    {
        $result = $this->profileBusiness->import($request->file('file'));

        return response()->json($result);
    }

    public function saveImport(Request $request)
    {
        $result = $this->profileBusiness->saveImport($request->profile_list, $request->city_id, $request->district_id);

        return response()->json($result);
    }

    public function storeFee(Request $request)
    {
        $request->validate([
            'profile_id' => 'required|numeric',
            'money' => 'required|numeric',
            'explain' => 'required'
        ]);

        return response()->json(
            $this->accountantBusiness->store(
                array_merge($request->except(['_token']), [
                    'type_id' => Accountant::INCOME_ID,
                ])
            )
        );
    }
}
