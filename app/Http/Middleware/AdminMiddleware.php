<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle($request, Closure $next, ...$roles)
    {
        if (Auth::check()) {
            if (Auth::user()->isAdmin() || $roles == 'all' || empty($roles)) {
                return $next($request);
            }

            if (Auth::user()->isMarketer() && in_array('marketer', $roles)) {
                return $next($request);
            }

            if (Auth::user()->isTeleSale() && in_array('tele_sale', $roles)) {
                return $next($request);
            }

            if (Auth::user()->isAccountant() && in_array('accountant', $roles)) {
                return $next($request);
            }

            abort(404);
        } else {
            return redirect()->route('login');
        }
    }
}
