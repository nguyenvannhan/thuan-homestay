<?php

namespace App\Http\Requests;

use App\Rules\ProfilePhone;
use Illuminate\Foundation\Http\FormRequest;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:250',
            'email' => 'nullable|unique:profiles,email,' . Request()->id . '|email',
            'phone' => ['nullable', new ProfilePhone(Request()->id)],
        ];
    }
}
