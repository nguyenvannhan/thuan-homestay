<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|file|mimes:xls,xlsx,csv'
        ];
    }

    public function messages()
    {
        return [
            'file.required' => 'Vui lòng chọn file!',
            'file.file' => 'Input nhập vào phải là file',
            'file.mimes' => 'Phần mở rộng của file phải là xls, xlsx hoặc csv',
        ];
    }
}
