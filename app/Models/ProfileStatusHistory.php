<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileStatusHistory extends Model
{
    protected $table = 'profile_status_histories';

    protected $fillable = [
        'profile_id', 'status_id', 'admin_id'
    ];

    protected static function booted()
    {
        static::creating(function ($profileStatus) {
            $profileStatus->fill([
                'admin_id' => Auth::id()
            ]);
        });

        static::updating(function ($profileStatus) {
            $profileStatus->fill([
                'admin_id' => Auth::id()
            ]);
        });
    }

    /******* RELATIONSHIP *********/
    public function profile() {
        return $this->belongsTo('App\Models\Profile');
    }
    /******* END RELATIONSHIP *********/
}
