<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Accountant extends Model
{
    use SoftDeletes;

    const INCOME_ID = 1;
    const OUTCOME_ID = 2;
    const DEPOSIT_ID = 3;

    const ACCOUNTANT_CREATE = 'accountants_create';

    protected static function booted()
    {
        static::creating(function ($profile) {
            $profile->fill([
                'user_id' => Auth::id()
            ]);
        });

        static::updating(function ($profile) {
            $profile->fill([
                'user_id' => Auth::id()
            ]);
        });
    }

    protected $table = 'accountants';

    protected $fillable = [
        'profile_id', 'type_id', 'user_id', 'money', 'explain'
    ];

    protected $casts = [
        'created_at' => 'datetime:d/m/Y H:i:s',
        'updated_at' => 'datetime:d/m/Y H:i:s',
    ];

    /** Relationship  n - 1 */
    public function profile()
    {
        return $this->belongsTo('App\Models\Profile');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /** Scopes */
    public function scopeOnlyIncome(Builder $builder)
    {
        return $builder->whereIn('type_id', [self::INCOME_ID, self::DEPOSIT_ID]);
    }

    public function scopeOnlyOutcome(Builder $builder)
    {
        return $builder->where('type_id', self::OUTCOME_ID);
    }
}
