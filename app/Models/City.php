<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    const HOCHIMINH_ID = 1;

    protected $fillable = ['name'];

    public function getDisplayNameAttribute()
    {
        return "{$this->prefix} {$this->name}";
    }

    /** Relationship 1 - n */
    public function districts()
    {
        return $this->hasMany('App\Models\District');
    }

    public function profiles()
    {
        return $this->hasMany('App\Models\Profile');
    }
}
