<?php

namespace App\Models;

use App\Models\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, HasRoles;

    // const ROLE_ADMIN = 0;
    // const ROLE_MARKETING = 1;
    // const ROLE_TELE_SALE = 2;
    // const ROLE_ACCOUNTANT = 3;

    // const ROLE_LIST = [
    //     self::ROLE_ADMIN => 'Admin',
    //     self::ROLE_MARKETING => 'Marketer',
    //     self::ROLE_TELE_SALE => 'TeleSale',
    //     self::ROLE_ACCOUNTANT => 'Accountant',
    // ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $with = ['roles.permissions', 'permissions'];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /** Relationship 1 - n */
    public function accountants()
    {
        return $this->hasMany('App\Models\Accountant');
    }
}
