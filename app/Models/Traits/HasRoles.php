<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;
use Spatie\Permission\Traits\HasRoles as BaseHasRoles;

trait HasRoles
{
    use BaseHasRoles;

    public function scopeExcludeAdmin(Builder $query)
    {
        $query->whereDoesntHave('roles', function ($roleQuery) {
            $roleQuery->where('name', 'admin');
        });
    }

    // public function isAdmin()
    // {
    //     return (int)$this->role_id === self::ROLE_ADMIN;
    // }

    // public function isMarketer()
    // {
    //     return (int)$this->role_id === self::ROLE_MARKETING || $this->isAdmin();
    // }

    // public function isTeleSale()
    // {
    //     return (int)$this->role_id === self::ROLE_TELE_SALE || $this->isAdmin();
    // }

    // public function isAccountant()
    // {
    //     return (int)$this->role_id === self::ROLE_ACCOUNTANT || $this->isAdmin();
    // }

    // public function scopeIsMarketer(Builder $builder)
    // {
    //     return $builder->where(function ($roleQuery) {
    //         $roleQuery->orWhere('role_id', self::ROLE_ADMIN)->orWhere('role_id', self::ROLE_MARKETING);
    //     });
    // }

    // public function scopeIsTeleSale(Builder $builder)
    // {
    //     return $builder->where(function ($roleQuery) {
    //         $roleQuery->orWhere('role_id', self::ROLE_ADMIN)->orWhere('role_id', self::ROLE_TELE_SALE);
    //     });
    // }

    // public function scopeIsAccountant(Builder $builder)
    // {
    //     return $builder->where(function ($roleQuery) {
    //         $roleQuery->orWhere('role_id', self::ROLE_ADMIN)->orWhere('role_id', self::ROLE_ACCOUNTANT);
    //     });
    // }

    // public function scopeOnlyMarketer(Builder $builder)
    // {
    //     $builder->where('role_id', self::ROLE_MARKETING);
    // }

    // public function scopeOnlyTeleSale(Builder $builder)
    // {
    //     $builder->where('role_id', self::ROLE_TELE_SALE);
    // }
}
