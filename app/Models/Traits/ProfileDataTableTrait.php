<?php

namespace App\Models\Traits;

trait ProfileDataTableTrait
{
    public function getProfileInfoAttribute()
    {
        $content =
            'Name: <strong>' . $this->name . '</strong><br>' .
            'Email: <strong>' . $this->email . '</strong><br>' .
            'Phone: <strong>' . $this->phone . '</strong><br>' .
            'Address: ' . $this->full_address . '<br>' .
            '<span class="text-primary">Created at: <strong>' . $this->created_at->format('d/m/Y H:i:s') . '</strong></span><br>' .
            '<span class="text-danger">Last Updated at: <strong>' . $this->updated_at->format('d/m/Y H:i:s') . '</strong></span><br>';

        if ($this->isRegistered() && $this->paid_fee->sum('money')) {
            $content .= '<span class="text-success">Học phí đã đóng: <strong>' . number_format($this->paid_fee->sum('money'), 0, '.', ',') . ' VNĐ</strong></span>';

            if ($this->paid_fee->sum('money')) {
                $content .= '<button class="fee-detail btn btn-link" data-profile-id="' . $this->id . '">Chi tiết học phí</button>';
            }
        }

        if ($this->isDeposited() && $this->deposited_fee) {
            $content .= '<span class="text-info">Đặt cọc: <strong>' . number_format($this->deposited_fee->money, 0, '.', ',') . ' VNĐ</strong></span>';

            if ($this->deposited_fee->money) {
                $content .= '<button class="deposit-edit btn btn-link" data-accountant-id="' . $this->deposited_fee->id . '" data-profile-id="' . $this->id . '">Chỉnh sửa</button>';
            }
        }

        return $content;
    }
}
