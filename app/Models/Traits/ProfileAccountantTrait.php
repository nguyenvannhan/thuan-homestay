<?php

namespace App\Models\Traits;

trait ProfileAccountantTrait
{
    public function isRegistered()
    {
        return $this->status_id == self::DA_DANG_KY;
    }

    public function isDeposited()
    {
        return $this->status_id == self::MUON_COC;
    }
}
