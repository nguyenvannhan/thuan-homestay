<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = ['name', 'city_id'];

    public function getDisplayNameAttribute()
    {
        return "{$this->prefix} {$this->name}";
    }

    /** Relationship n - 1 */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    /** Relationship 1 - n */
    public function profiles()
    {
        return $this->hasMany('App\Models\Profile');
    }
}
