<?php

namespace App\Rules;

use App\Core\Repository\Contracts\ProfileInterface;
use Illuminate\Contracts\Validation\Rule;

class ProfilePhone implements Rule
{
    private $profileRepository;

    private $except_ids = [];

    private $errorMessage;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($except_ids = [])
    {
        $this->profileRepository = resolve(ProfileInterface::class);
        if (is_array($except_ids)) {
            $this->except_ids = $except_ids;
        } else {
            $this->except_ids = explode(',', $except_ids);
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->profileRepository->checkPhoneListExisted($value, $this->except_ids)) {
            $this->errorMessage = 'SĐT đã tồn tại trong dữ liệu!';
            return false;
        };

        if (!is_array($value)) {
            $phoneList = \phone_str_to_array($value);
        } else {
            $phoneList = $value;
        }

        $uniqueValues = [];

        foreach ($phoneList as $phone) {
            if (\in_array($phone, $uniqueValues)) {
                $this->errorMessage = 'SĐT ' . $phone . ' bị lặp lại!';
                return false;
            }

            if (\strlen($phone) != 10) {
                $this->errorMessage = 'SĐT ' . $phone . ' không đúng dạng 10 số!';
                return false;
            }

            $uniqueValues[] = $phone;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errorMessage;
    }
}
