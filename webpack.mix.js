const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/assets/js/accountant.js", "public/js");
mix.js("resources/assets/js/profile.js", "public/js")
mix.js("resources/assets/js/profile-import.js", "public/js")
    .js("resources/assets/js/user.js", "public/js")

    .options({ processUrls: false });

if (mix.inProduction()) {
    mix.version();
}
