<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false, 'reset' => false]);

Route::middleware('auth')->group(function () {
    Route::get('/change-password', 'AuthController@changePassword')->name('change_password');
    Route::post('/change-password', 'AuthController@postChangePassword');

    Route::get('/', 'HomeController@index')->name('index');

    Route::prefix('users')->name('user.')->group(function () {
        Route::middleware('can:users_list')->group(function () {
            Route::get('/', 'UserController@index')->name('index');
            Route::post('/data-table', 'UserController@getDataTable');
        });

        Route::middleware('can:users_create')->group(function () {
            Route::get('/create', 'UserController@create')->name('create');
            Route::post('/store', 'UserController@store')->name('store');
        });

        Route::middleware('can:users_update')->group(function () {
            Route::get('/edit/{id}', 'UserController@edit')->name('edit');
            Route::post('/edit/{id}', 'UserController@update')->name('update');
        });

        Route::post('change-status', 'UserController@changeStatus')->middleware('can:users_change_status');
        Route::post('/reset-password', 'UserController@resetPassword')->middleware('can:users_reset_password');
    });

    Route::prefix('profiles')->name('profile.')->group(function () {
        Route::post('/data-table', 'ProfileController@getDataTable');
        Route::get('/', 'ProfileController@index')->name('index');

        Route::middleware('can:profiles_import')->group(function () {
            Route::get('/create', 'ProfileController@create')->name('create');
            Route::post('/create', 'ProfileController@store')->name('store');
        });

        Route::middleware('can:profiles_import')->group(function () {
            Route::get('/import', 'ProfileController@getImportFile')->name('import');
            Route::post('/import', 'ProfileController@postImportFile');
            Route::post('/import-save', 'ProfileController@saveImport');
        });

        Route::middleware('can:profiles_update')->group(function () {
            Route::get('edit/{id}', 'ProfileController@edit')->name('edit');
            Route::post('edit/{id}', 'ProfileController@update')->name('update');
        });

        Route::post('/delete', 'ProfileController@delete')->middleware('role:admin');

        Route::middleware('can:accountants_create')->name('fee.')->group(function () {
            Route::post('fee', 'ProfileController@storeFee')->name('store');
        });

        Route::get('/{id}', 'ProfileController@show')->name('show');
    });

    Route::prefix('accountants')->name('accountant.')->group(function () {
        Route::middleware('can:accountants_list')->group(function () {
            Route::get('/', 'AccountantController@index')->name('index');
            Route::post('income/data-table', 'AccountantController@getIncomeDataTable');
            Route::post('outcome/data-table', 'AccountantController@getOutcomeDataTable');

            Route::post('get-sum', 'AccountantController@getSum');
            Route::post('profiles/{id}', 'AccountantController@profile');
        });

        Route::middleware('can:accountants_create')->group(function () {
            Route::get('/create', 'AccountantController@create')->name('create');
            Route::post('/store', 'AccountantController@store')->name('store');
        });

        Route::middleware('can:accountants_update')->group(function () {
            Route::get('/edit/{id}', 'AccountantController@edit')->name('edit');
            Route::post('/edit/{id}', 'AccountantController@update')->name('update');
        });

        Route::post('/delete', 'AccountantController@delete')->middleware('role:admin');

        Route::prefix('ajax')->group(function () {
            Route::get('deposit', 'AccountantController@ajaxShowDeposit');
            Route::post('deposit', 'AccountantController@ajaxUpdateDeposit');
        });
    });

    Route::prefix('district')->name('district.')->group(function () {
        Route::get('/get-from-city/{city_id}', 'DistrictController@getFromCity');
    });
});
