<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Profile;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {
    $districtItem = $faker->boolean(50) ? \App\Models\District::inRandomOrder()->first() : null;

    return [
        'email' => $faker->boolean(50) ? $faker->email : null,
        'phone' => $faker->phoneNumber,
        'name' => $faker->name,
        'address' => $faker->boolean(50) ? $faker->address : null,
        'city_id' => $districtItem ? $districtItem->city_id : 0,
        'district_id' => $districtItem ? $districtItem->id : 0,
        'appointment_time' => $faker->boolean(50) ? $faker->dateTime->format('Y-m-d H:i:s') : null,
        'status_id' => array_rand(Profile::STATUS_LIST),
        'marketer_id' => User::whereHas('roles', function ($roleQuery) {
            $roleQuery->whereIn('name', ['admin', 'marketer']);
        })->inRandomOrder()->first()->id,
        'tele_sale_id' => $faker->boolean(50) ? User::whereHas('roles', function ($roleQuery) {
            $roleQuery->whereIn('name', ['admin', 'tele_sale']);
        })->inRandomOrder()->first()->id : 0,
        'note' => $faker->boolean(50) ? $faker->realText() : null
    ];
});
