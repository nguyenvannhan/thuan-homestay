<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                //1,
                'name' => 'Hồ Chí Minh',
                'prefix' => 'Thành phố'
            ],
            [
                //2,
                'name' => 'Hà Nội',
                'prefix' => 'Thành phố'
            ],
            [
                //3,
                'name' => 'Đà Nẵng',
                'prefix' => 'Thành phố'
            ],
            [
                //4,
                'name' => 'Bình Dương',
                'prefix' => 'Tỉnh'
            ],
            [
                //5,
                'name' => 'Đồng Nai',
                'prefix' => 'Tỉnh'
            ],
            [
                //6,
                'name' => 'Khánh Hòa',
                'prefix' => 'Tỉnh'
            ],
            [
                //7,
                'name' => 'Hải Phòng',
                'prefix' => 'Thành phố'
            ],
            [
                //8,
                'name' => 'Long An',
                'prefix' => 'Tỉnh'
            ],
            [
                //9,
                'name' => 'Quảng Nam',
                'prefix' => 'Tỉnh'
            ],
            [
                //10,
                'name' => 'Bà Rịa Vũng Tàu',
                'prefix' => 'Tỉnh'
            ],
            [
                //11,
                'name' => 'Đắk Lắk',
                'prefix' => 'Tỉnh'
            ],
            [
                //12,
                'name' => 'Cần Thơ',
                'prefix' => 'Thành phố'
            ],
            [
                //13,
                'name' => 'Bình Thuận',
                'prefix' => 'Tỉnh'
            ],
            [
                //14,
                'name' => 'Lâm Đồng',
                'prefix' => 'Tỉnh'
            ],
            [
                //15,
                'name' => 'Thừa Thiên Huế',
                'prefix' => 'Tỉnh'
            ],
            [
                //16,
                'name' => 'Kiên Giang',
                'prefix' => 'Tỉnh'
            ],
            [
                //17,
                'name' => 'Bắc Ninh',
                'prefix' => 'Tỉnh'
            ],
            [
                //18,
                'name' => 'Quảng Ninh',
                'prefix' => 'Tỉnh'
            ],
            [
                //19,
                'name' => 'Thanh Hóa',
                'prefix' => 'Tỉnh'
            ],
            [
                //20,
                'name' => 'Nghệ An',
                'prefix' => 'Tỉnh'
            ],
            [
                //21,
                'name' => 'Hải Dương',
                'prefix' => 'Tỉnh'
            ],
            [
                //22,
                'name' => 'Gia Lai',
                'prefix' => 'Tỉnh'
            ],
            [
                //23,
                'name' => 'Bình Phước',
                'prefix' => 'Tỉnh'
            ],
            [
                //24,
                'name' => 'Hưng Yên',
                'prefix' => 'Tỉnh'
            ],
            [
                //25,
                'name' => 'Bình Định',
                'prefix' => 'Tỉnh'
            ],
            [
                //26,
                'name' => 'Tiền Giang',
                'prefix' => 'Tỉnh'
            ],
            [
                //27,
                'name' => 'Thái Bình',
                'prefix' => 'Tỉnh'
            ],
            [
                //28,
                'name' => 'Bắc Giang',
                'prefix' => 'Tỉnh'
            ],
            [
                //29,
                'name' => 'Hòa Bình',
                'prefix' => 'Tỉnh'
            ],
            [
                //30,
                'name' => 'An Giang',
                'prefix' => 'Tỉnh'
            ],
            [
                //31,
                'name' => 'Vĩnh Phúc',
                'prefix' => 'Tỉnh'
            ],
            [
                //32,
                'name' => 'Tây Ninh',
                'prefix' => 'Tỉnh'
            ],
            [
                //33,
                'name' => 'Thái Nguyên',
                'prefix' => 'Tỉnh'
            ],
            [
                //34,
                'name' => 'Lào Cai',
                'prefix' => 'Tỉnh'
            ],
            [
                //35,
                'name' => 'Nam Định',
                'prefix' => 'Tỉnh'
            ],
            [
                //36,
                'name' => 'Quảng Ngãi',
                'prefix' => 'Tỉnh'
            ],
            [
                //37,
                'name' => 'Bến Tre',
                'prefix' => 'Tỉnh'
            ],
            [
                //38,
                'name' => 'Đắk Nông',
                'prefix' => 'Tỉnh'
            ],
            [
                //39,
                'name' => 'Cà Mau',
                'prefix' => 'Tỉnh'
            ],
            [
                //40,
                'name' => 'Vĩnh Long',
                'prefix' => 'Tỉnh'
            ],
            [
                //41,
                'name' => 'Ninh Bình',
                'prefix' => 'Tỉnh'
            ],
            [
                //42,
                'name' => 'Phú Thọ',
                'prefix' => 'Tỉnh'
            ],
            [
                //43,
                'name' => 'Ninh Thuận',
                'prefix' => 'Tỉnh'
            ],
            [
                //44,
                'name' => 'Phú Yên',
                'prefix' => 'Tỉnh'
            ],
            [
                //45,
                'name' => 'Hà Nam',
                'prefix' => 'Tỉnh'
            ],
            [
                //46,
                'name' => 'Hà Tĩnh',
                'prefix' => 'Tỉnh'
            ],
            [
                //47,
                'name' => 'Đồng Tháp',
                'prefix' => 'Tỉnh'
            ],
            [
                //48,
                'name' => 'Sóc Trăng',
                'prefix' => 'Tỉnh'
            ],
            [
                //49,
                'name' => 'Kon Tum',
                'prefix' => 'Tỉnh'
            ],
            [
                //50,
                'name' => 'Quảng Bình',
                'prefix' => 'Tỉnh'
            ],
            [
                //51,
                'name' => 'Quảng Trị',
                'prefix' => 'Tỉnh'
            ],
            [
                //52,
                'name' => 'Trà Vinh',
                'prefix' => 'Tỉnh'
            ],
            [
                //53,
                'name' => 'Hậu Giang',
                'prefix' => 'Tỉnh'
            ],
            [
                //54,
                'name' => 'Sơn La',
                'prefix' => 'Tỉnh'
            ],
            [
                //55,
                'name' => 'Bạc Liêu',
                'prefix' => 'Tỉnh'
            ],
            [
                //56,
                'name' => 'Yên Bái',
                'prefix' => 'Tỉnh'
            ],
            [
                //57,
                'name' => 'Tuyên Quang',
                'prefix' => 'Tỉnh'
            ],
            [
                //58,
                'name' => 'Điện Biên',
                'prefix' => 'Tỉnh'
            ],
            [
                //59,
                'name' => 'Lai Châu',
                'prefix' => 'Tỉnh'
            ],
            [
                //60,
                'name' => 'Lạng Sơn',
                'prefix' => 'Tỉnh'
            ],
            [
                //61,
                'name' => 'Hà Giang',
                'prefix' => 'Tỉnh'
            ],
            [
                //62,
                'name' => 'Bắc Kạn',
                'prefix' => 'Tỉnh'
            ],
            [
                //63,
                'name' => 'Cao Bằng',
                'prefix' => 'Tỉnh'
            ],
        ];

        foreach($data as $city) {
            \App\Models\City::create($city);
        }
    }
}
