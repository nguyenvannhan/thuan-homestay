<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::flushEventListeners();

        factory(App\Models\User::class, 50)->create();

        \App\Models\User::create([
            'name' => 'Admin',
            'email' => 'nguyenvannhan0810@gmail.com',
            'password' => 'nhan@123456'
        ])->assignRole('admin');

        \App\Models\User::create([
            'name' => 'Profile Admin',
            'email' => 'profileg@gmail.com',
            'password' => 'nhan@123456',
        ])->assignRole('profiles');

        \App\Models\User::create([
            'name' => 'User Admin',
            'email' => 'user@gmail.com',
            'password' => 'nhan@123456',
        ])->assignRole('users');
        \App\Models\User::create([
            'name' => 'Accountant Admin',
            'email' => 'accountant@gmail.com',
            'password' => 'nhan@123456',
        ])->assignRole('accountants');
    }
}
