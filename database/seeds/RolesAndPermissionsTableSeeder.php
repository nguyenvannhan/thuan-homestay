<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesAndPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $permissions = [
            //User Permissions
            ['name' => 'users_list'],
            ['name' => 'users_create'],
            ['name' => 'users_update'],
            ['name' => 'users_reset_password'],
            ['name' => 'users_change_status'],

            // Profile Permissions
            ['name' => 'profiles_create'],
            ['name' => 'profiles_update'],
            ['name' => 'profiles_import'],
            // Accountant Permission
            ['name' => 'accountants_list'],
            ['name' => 'accountants_create'],
            ['name' => 'accountants_update'],
        ];

        $roles = [
            [
                'name' => 'admin',
                'permissions' => 'full'
            ],
            [
                'name' => 'users',
                'permissions' => [
                    'users_list', 'users_create', 'users_update', 'users_reset_password', 'users_change_status'
                ]
            ],
            [
                'name' => 'profiles',
                'permissions' => [
                    'profiles_create', 'profiles_import', 'profiles_update'
                ]
            ],
            [
                'name' => 'accountants',
                'permissions' => [
                    'accountants_list', 'accountants_create', 'accountants_update'
                ]
            ],
        ];

        $permissionList = collect([]);

        foreach ($permissions as $permission) {
            $permissionList->push(Permission::create($permission));
        }

        foreach ($roles as $role) {
            $rolePermissions = $role['permissions'];

            unset($role['permissions']);

            $role = Role::create($role);
            if ($rolePermissions === 'full') {
                $role->givePermissionTo($permissionList);
            } else {
                $role->givePermissionTo($rolePermissions);
            }
        }
    }
}
